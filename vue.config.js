const hostURL = process.env.HOST_URL;
const Timestamp = new Date().getTime();
module.exports = {
  publicPath:"/Public/home/yq/src/xc/",
  assetsDir: "static", 
  outputDir: "dist",
  assetsDir:"static",
  indexPath:'indexNew.html',
  productionSourceMap:false,
  configureWebpack: {
    output: { // 输出重构  打包编译后的 文件名称  【模块名称.版本号.时间戳】
      filename: `js/[name].${Timestamp}.js`,
      chunkFilename: `js/[name].${Timestamp}.js`
    },
    resolve: {
      alias: {
        'assets': '@/assets',
        'components': '@/components',
        'views': '@/views',
      }
    }

  },
  css: {
    loaderOptions: {
      css: {
        // 这里的选项会传递给 css-loader
        importLoaders: 1,
      },
      less: {
        // 这里的选项会传递给 postcss-loader
        importLoaders: 1,
      }
    }
  },
  devServer: {
    open: process.platform === 'vue',
    // host: '127.0.0.1',
    // port: 8080,
    https: false,
    hotOnly: false,
    proxy: { // 设置代理
      '/api': {
        // target: '/api/index.php/',
        target: 'http://yq.wisedata.cc',
        changeOrigin: true,
        pathRewrite: {
          '^/api': ''
        }
      },
      '/mapApi': {
        target: 'http://yq.wisedata.cc/Public/home/yq/src/map/',
        changeOrigin: true,
        pathRewrite: {
          '^/mapApi': ''
        }
      }
    },
    disableHostCheck: true
  },
  chainWebpack: config => {
    // 修复HMR
    config.resolve.symlinks(true);
  }
}