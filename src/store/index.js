import base from './_base/index';

// 模块
import dashboard from './dashboard/index';

// 通过模块名来获取模块状态: store[MODULE_NAME].states
const modules = { dashboard };

// 注入 modules，给模块名添加 'Model' 后缀
for (const key in modules) {
  base[`${key}Model`] = modules[key];
}

export const mapActions = () => {
  return base.actions;
};

export const mapMutations = () => {
  return base.mutations;
};

export default base;
