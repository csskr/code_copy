import Vue from 'vue';

export default class Store {
  constructor({ states, actions, mutations }) {
    // 状态
    this.states = Vue.observable(states || {});
    // 异步函数
    this.actions = Vue.observable(actions || {});
    // 同步函数
    this.mutations = Vue.observable(mutations || {});
  }
  // 调用 mutations 中的同步函数
  commit = (fun, params) => {
    if (fun) {
      return this.mutations[fun].call(this, params);
    } else {
      return false;
    }
  };
  // 调用 actions 中的异步函数
  dispatch = (fun, params) => {
    if (fun) {
      return this.actions[fun].call(this, params);
    } else {
      return false;
    }
  };

  /**
   * 更新 states 的状态
   * @param {Boolean} assign 合并还是覆盖，默认覆盖
   */
  update = (key, value, assign = false) => {
    if (key) {
      if (assign &&
        Object.prototype.toString.call(this.states[key]) === '[object Object]' &&
        Object.prototype.toString.call(value) === '[object Object]'
      ) {
        const res = Object.assign({}, this.states[key], value);
        this.states[key] = res;
      } else {
        this.states[key] = value;
      }
    } else {
      return false;
    }
  };
}
