import { atobQuery, saveToStorage, isObject } from '../../util/util';

// 客户端交互
import extensions from './webview/extensions';

/* 所有函数的入参只能有一个，如需多个入参，可采用对象解构 */
export default {
  ...extensions,
  /**
   * 从 URI 获取用户信息
   * 在运营后台嵌套本项目的时候会用到
   * 允升: user_id=1&realname=yunsom&enterprise_id=1&phone=02386663188&dashboard_size=nomarl&client_type=2
   * 加密: dXNlcl9pZD0xJnJlYWxuYW1lPXl1bnNvbSZlbnRlcnByaXNlX2lkPTEmcGhvbmU9MDIzODY2NjMxODgmZGFzaGJvYXJkX3NpemU9bm9tYXJsJmNsaWVudF90eXBlPTI%3D
   */
  getUserInfoFromURI() {
    const search = window.location.search;
    if (!search) { return; }

    const data = atobQuery(search.substr(1));
    // 将用户信息保存到 sessionStorage
    if (isObject(data)) {
      for (const key in data) {
        saveToStorage(key, data[key]);
      }
    }

    return { data };
  },
};
