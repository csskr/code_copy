// 接口
import axios from 'axios';
import { WISE_USER } from '../../api/config';
export default {

  async getUserInfo() {
    console.log('login')
    const res = await axios({
      method: 'post',
      url: '/api/index.php/Login/CheckUserAjax.html',
      // url: '/api/Login/CheckUserAjax.html',
      data: WISE_USER,
      transformRequest: [
        function(data) {
          let ret = ''
          for (let it in data) {
              ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
          }
          return ret
        }
      ],
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    })
    return res;
    // console.log(res)
  },

};
