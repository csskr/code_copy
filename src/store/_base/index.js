import Store from '../store';

import states from './states';
import actions from './actions';
import mutations from './mutations';

const BaseStore = new Store({
  states,
  actions,
  mutations,
});

export default BaseStore;
