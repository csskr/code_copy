// 接口
import dashboard from '../../api/dashboard/index';
// import dashboard from '../../api/dashboard_dev/index';
// mock，需要在 url 上将仪表盘 id 改为 666
import mockData from '../../util/mock';

const isPro = true || process.env.NODE_ENV === 'production';
const Mock = isPro ? {
        // detail: mockData.search
    } :
    mockData; // mock

const $api = {
    dashboard,
};

/* 所有函数的入参只能有一个，如需多个入参，可采用对象解构 */
export default {
    // 查询仪表盘组的详情
    async getNews() {
        const { data } = Mock.group || (await $api.dashboard.getNews());
        if (data) {
            this.update('newsList', data)
            return data;
        }
    },
    async getEmotionData() {
        const { data } = Mock.group || (await $api.dashboard.getEmotionData());
        if (data) {
            this.update('emotionsList', data)
            return data;
        }
    },
    async getFastEvent() {
        const { data } = Mock.group || (await $api.dashboard.getFastEvent());
        if (data) {
            this.update('FastEventList', data)
            return data;
        }
    },
    async getSiteData() {
        const { data } = (await $api.dashboard.getSiteData());
        if (data) {
            this.update('getSiteDataList', data)
            return data;
        }
    },
    async getAreaheat() {
        const { data } = Mock.group || (await $api.dashboard.getAreaheat());
        if (data) {
            this.update('AreaheatList', data)
            return data;
        }
    },
    // async getMapJson() {
    //     const { data } = Mock.group || (await $api.dashboard.getMapJson());
    //     if (data) {
         
    //         this.update('MapJson', data)
    //         return data;
    //     }
    // },
    async getEmotionalTrend(){
        const { data } = Mock.group || (await $api.dashboard.getEmotionalTrend());
        if (data) {
            this.update('EmotionalTrendList', data)
            return data;
        }
    },
    async getMediaData(){
        const { data } = Mock.group || (await $api.dashboard.getMediaData());
        if (data) {
            this.update('MediaDataList', data)
            return data;
        }
    },
    async getMapTab(){
        const { data } = Mock.group || (await $api.dashboard.getMapTab());
        if (data) {
            this.update('MapTabJson', data)
            return data;
        }
    },
    
    
};