// import moment from 'moment';
import { isArray } from '../../util/util';

/* 所有函数的入参只能有一个，如需多个入参，可采用对象解构 */
export default {
    /**
     * 清除 store 中的 state
     * 组件销毁时必须调用对应 store 的销毁方法，否则会产生内存泄漏
     */
    destroy() {
        this.update('dashboardDetail', {});
    },

    /**
     * 设置文本组件的入参
     */
    setTextConditionMap(data) {
        if (!isArray(data)) {
            return;
        }
        this.update('newsList', data)
        this.update('emotionsList', data)
        this.update('FastEventList', data)
    },
};