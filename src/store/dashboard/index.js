import Store from '../store';

import states from './states';
import actions from './actions';
import mutations from './mutations';

const Dashboardstore = new Store({
  states,
  actions,
  mutations,
});

export default Dashboardstore;
