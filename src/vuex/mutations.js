const mutations = {
    increment(state, payload) {
        state.count += payload;
    },
    updateCount(state) {
        state.count--
    },
    setLang(state, payload){
        state.lang=payload
    },
    setTabs(state, pramas){
        state.editableTabs = pramas;
    },
    setCityName(state, pramas){
        state.cityName = pramas;
    }
}
export default mutations