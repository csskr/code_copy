/* 通过 Vue.use() 将请求方法挂到 Vue 原型链（暂未使用）*/
import user from './user/index';
import dashboard from './dashboard/index';
// import dashboard from './dashboard_dev/index';
const install = function(Vue) {
  if (install.installed) {
    return;
  }
  install.installed = true;

  // 为 Vue 实例添加 $api 属性
  // 在组件中以 this.$api.dashboard.detail(params) 的方式发起请求
  Object.defineProperties(Vue.prototype, {
    $api: {
      get() {
        return { user, dashboard };
      },
    },
  });
};

export default {
  install,
};
