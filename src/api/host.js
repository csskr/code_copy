// 各个环境的域名，用于设置 BaseURL
const DOMAIN = {
  DEV: '.wisedata.cc',
  TEST: '.wisedata.cc',
  PRO: '.wisedata.cc',
};

export const BASE_URL = {
  DEV: /api+'index.php',
  PRO: /api+'index.php',
};

// 设置 BaseURL 的地址
const setBaseHost = () => {
  const hostname = window.location.hostname;

  let host = '';
  switch (true) {
    // 测试环境
    case hostname.indexOf(DOMAIN.DEV) !== -1:
      host = BASE_URL.DEV;
      break;
    // 测试环境2
    case hostname.indexOf(DOMAIN.TEST2) !== -1:
      host = BASE_URL.TEST2;
      break;
    // 测试环境
    case hostname.indexOf(DOMAIN.TEST) !== -1:
      host = BASE_URL.TEST;
      break;
    // 正式环境
    case hostname.indexOf(DOMAIN.PRO) !== -1:
      host = BASE_URL.PRO;
      break;
    // 本地
    default:
      host = '';
      break;
  }

  return host;
};

export default setBaseHost();
