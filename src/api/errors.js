export class ApiError extends Error {
  constructor(message, url) {
    super(message);
    this.message = message;
    this.name = 'ApiError';
    this.url = url;

    this.constructor = ApiError;
    Object.setPrototypeOf(this, ApiError.prototype);
  }
}
export class ApiTimeoutError extends ApiError {
  constructor(time, url) {
    super('请求超时', url);
    this.name = 'ApiTimeoutError';
    this.time = time;

    this.constructor = ApiTimeoutError;
    Object.setPrototypeOf(this, ApiTimeoutError.prototype);
  }
}
export class ApiServerError extends ApiError {
  constructor(statusCode, url) {
    super(`请求服务器出错：${statusCode}`, url);
    this.name = 'ApiServerError';
    this.statusCode = statusCode;
    this.constructor = ApiServerError;
    Object.setPrototypeOf(this, ApiServerError.prototype);
  }
}
export class ApiModuleError extends ApiError {
  constructor(statusCode, url) {
    super(`模块服务不可用：${statusCode}`, url);
    this.name = 'ApiModuleError';
    this.constructor = ApiModuleError;
    Object.setPrototypeOf(this, ApiModuleError.prototype);
  }
}

export class ApiAuthError extends ApiError {
  constructor(url) {
    super('登录已过期', url);
    this.name = 'ApiAuthError';
    this.constructor = ApiAuthError;
    Object.setPrototypeOf(this, ApiAuthError.prototype);
  }
}

export class ApiJsonError extends ApiError {
  constructor(url) {
    super(`请求服务器出错：无法转换为JSON`, url);
    this.name = 'ApiJsonError';
    this.constructor = ApiJsonError;
    Object.setPrototypeOf(this, ApiJsonError.prototype);
  }
}

export class ApiBusinessError extends ApiError {
  constructor(message, url, response) {
    super(message, url);
    this.name = 'ApiBusinessError';
    this.constructor = ApiBusinessError;
    Object.setPrototypeOf(this, ApiBusinessError.prototype);
    this.response = response;
  }
}
