import axios from './axios';

const request = axios();

// 封装 get、post、put、delete 常用方法
export default {
  get(url, params = {}) {
    return request.get(url, { params });
  },

  post(url, params) {
    return request.post(url, params);
  },

  put(url, params) {
    return request.put(url, params);
  },

  delete(url, params) {
    return request.delete(url, params);
  },
};
