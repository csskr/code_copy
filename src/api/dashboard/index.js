import $http from '../index';

export default {
    // 登录
    Login(params) {
        // return $http.post('/api/index.php/Login/CheckUserAjax.html`, params);
        return console.log(params)
    },
    // 最新事件
    getNews() {
        return $http.get('/api/index.php/Vision/getNews');
    },
    // 情感分析
    getEmotionData() {
        return $http.get('/api/index.php/Vision/getEmotionData');
    },
    // 应急事件
    getFastEvent() {
        return $http.get('/api/index.php/Vision/getFastEvent');
    },
    //
    getSiteData(){
        return $http.post('/api/index.php/Vision/getSiteData');
    },
    // 輿情熱度
    getAreaheat() {
        return $http.get('/api/index.php/Vision/getAreaheat');
    },
    // // 地圖
    // getMapJson() {
    //     return $http.get(/api+'Public/home/yq/src/map//330100.json');
    // },
    // 情感趨勢
    getEmotionalTrend(){
        return $http.get('/api/index.php/Vision/getEmotionalTrend');
    },
    // 媒体分析
    getMediaData() {
        return $http.get('/api/index.php/Vision/getMediaData');
    },
    getMapTab(){
        return $http.get('/api/index.php/Vision/getMapTab');
    }
};