import axios from 'axios';
import { isObject } from '../util/util';
import errorHandler from '@/util/error';
// import baseURL from './host';

import {
  ApiAuthError,
  ApiBusinessError,
  ApiModuleError,
  ApiServerError,
  ApiTimeoutError,
} from './errors';

import { TIMEOUT, MIDDLE_TRUE_CODE, BACK_TRUE_CODE } from './config';

// 添加统一的入参
const buildParams = (params) => {
  const baseParams = {
    
  };
  return isObject(params)
    ? Object.assign({}, params, baseParams)
    : baseParams;
};

const headers = {
  'Content-Type': 'application/json',
};

// 创建 axios 实例
let service = axios.create({
  // baseURL,
  headers,
  timeout: TIMEOUT,
  withCredentials: true,
  validateStatus: function(status) {
    return status >= 200 && status < 300;
  },
});

// 添加请求拦截器
service.interceptors.request.use(
  config => {
    let { method, data, params = {} } = config;
    method = `${method}`.toUpperCase();

    if (method !== 'GET') {
      data = buildParams(data);
      // post、put、delete 提交时，将对象转换为string
      config.data = JSON.stringify(data);
    } else {
      // 增加时间戳 避免IE缓存 仅 GET
      params.t = Date.now();
      config.params = buildParams(params);
    }
    // 请求发送前进行处理
    return config;
  },
  error => {
    // 请求错误处理
    return Promise.reject(error);
  },
);

// 添加响应拦截器
service.interceptors.response.use(
  response => {
    const { data = {}, config = {} } = response;
    const { code, message } = data;

    if (code !== MIDDLE_TRUE_CODE && code !== BACK_TRUE_CODE) {
      const err = new ApiBusinessError(message, config.url, data);
      errorHandler(err, {}, {}, true);
      throw err;
    } else if (data.data && data.data.code) {
      // 中间层处理后端接口时，会将后端的响应包装在 data 内
      const backData = data.data;
      if (backData.code === BACK_TRUE_CODE) {
        return data.data;
      } else {
        const err = new ApiBusinessError(backData.message, config.url, backData);
        errorHandler(err, {}, {}, true);
        throw err;
      }
    } else {
      return data;
    }
  },
  error => {
    const { status, config } = error.response || error || {};

    // 超时
    if (error.code === 'ECONNABORTED' && error.message.indexOf('timeout') !== -1) {
      const err = new ApiTimeoutError(TIMEOUT, config.url);
      errorHandler(err, {}, {}, true);
      throw err;
    }

    let err;
    switch (Number(status)) {
      case 401:
        err = new ApiAuthError(config.url);
        break;
      case 503:
        err = new ApiModuleError(status, config.url);
        break;
      default:
        err = new ApiServerError(status, config.url);
        break;
    }
    errorHandler(err, {}, {}, true);
    throw err;
  },
);

/**
 * 创建统一封装过的 axios 实例
 * @return {AxiosInstance}
 */
export default function() {
  return service;
}
