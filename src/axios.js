import axios from 'axios'
import Qs from 'qs'
import {
    Message
} from 'element-ui';

const instance = axios.create({
    timeout: 60000,
    // baseURL: 'https://www.example.com/',
    headers: {
        "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
        "DaWang": 'v-1.0'
    },
    responseType: "json",
    withCredentials: true, // 是否允许带cookie这些
})
// 请求超时重新请求次数，请求间隙
instance.defaults.retry = 2
instance.defaults.retryDelay = 5000
instance.interceptors.request.use(
    config => {
        //动态token
        config.data = Qs.stringify(config.data);
        // config.params = config.data;//jAVA

        return config;
    },
    error => {
        return Promise.reject(error);
    }
);
instance.interceptors.response.use(
    response => {
        //接口返回更新token
        // if (response.headers.token) {
        //     localStorage.setItem('token', res.headers.token);
        // }
        return response;
    },
    error => {
        Message.error(error.response.data.msg  || '接口异常')
        return Promise.reject(error)
    }
)
/**
 * 封装get方法
 * @param url
 * @param data
 * @returns {Promise}
 */
export function get(url, params = {}) {
    return new Promise((resolve, reject) => {
        instance.get(url, {
                params: params
            })
            .then(response => {
                resolve(response.data);
            })
            .catch(err => {
                reject(err.response)
            })
    })
}
/**
 * 封装post请求
 * @param url
 * @param data
 * @returns {Promise}
 */

export function post(url, data) {
    return new Promise((resolve, reject) => {
        instance.post(url, data)
            .then(response => {
                resolve(response.data);
            }, err => {
                reject(err.response)
            })
    })
}

/**
 * 封装patch请求
 * @param url
 * @param data
 * @returns {Promise}
 */
export function patch(url, data = {}) {
    return new Promise((resolve, reject) => {
        instance.patch(url, data)
            .then(response => {
                resolve(response.data);
            }, err => {
                reject(err.response)
            })
    })
}
/**
 * 封装put请求
 * @param url
 * @param data
 * @returns {Promise}
 */

export function put(url, data = {}) {
    return new Promise((resolve, reject) => {
        instance.put(url, data)
            .then(response => {
                resolve(response.data);
            }, err => {
                reject(err.response)
            })
    })
}