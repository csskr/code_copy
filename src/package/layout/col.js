export default {
  name: 'YunCol',

  props: {
    // 栅格占据的列数，最大为24
    span: {
      type: Number,
      default: 24
    },
    // 栅格左侧间隔格数
    offset: Number,
    // 栅格向左移动格数
    pull: Number,
    // 栅格向右移动格数
    push: Number
  },

  computed: {
    gutter() {
      let parent = this.$parent;
      while (parent && parent.$options.componentName !== 'YunRow') {
        parent = parent.$parent;
      }
      return parent ? parent.gutter : 0;
    }
  },

  render(h) {
    let classList = [];
    let style = {};

    if (this.gutter) {
      style.paddingLeft = this.gutter / 2 + 'px';
      style.paddingRight = style.paddingLeft;
    }

    ['span', 'offset', 'pull', 'push'].forEach(prop => {
      if (this[prop] || this[prop] === 0) {
        classList.push(
          prop !== 'span'
            ? `yun-col-${prop}-${this[prop]}`
            : `yun-col-${this[prop]}`
        );
      }
    });

    return h(
      'div',
      {
        class: ['yun-col', classList],
        style
      },
      this.$slots.default
    );
  }
};
