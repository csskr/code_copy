export default {
  name: 'YunRow',

  componentName: 'YunRow',

  props: {
    // 设置 col 之间的间距
    gutter: Number
  },

  computed: {
    style() {
      const ret = {};

      if (this.gutter) {
        ret.marginLeft = `-${this.gutter / 2}px`;
        ret.marginRight = ret.marginLeft;
      }

      return ret;
    }
  },

  render(h) {
    return h(
      'div',
      {
        class: ['yun-row'],
        style: this.style
      },
      this.$slots.default
    );
  }
};
