// 按需引入elementUI
import {
  Button,
  Select,
  Option,
  Radio,
  RadioGroup,
  RadioButton,
  Dialog,
  Form,
  FormItem,
  Popover,
  Input,
  Tree,
  Scrollbar,
  DatePicker,
  Carousel,
  CarouselItem,
  Drawer,
  Rate
} from 'element-ui';

const element = [
  Button,
  Select,
  Option,
  Radio,
  RadioGroup,
  RadioButton,
  Dialog,
  Form,
  FormItem,
  Popover,
  Input,
  Tree,
  Scrollbar,
  DatePicker,
  Carousel,
  CarouselItem,
  Drawer,
  Rate
];

export default element;
