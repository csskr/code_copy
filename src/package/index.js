import YunCol from './layout/col';
import YunRow from './layout/row';
import YunWidget from './widget';
// import Charts from './charts';
// import ElementUI from './element-ui';
import { Loading } from 'element-ui';

const components = [YunRow, YunCol, YunWidget];

// 暴露 install 方法，用于组件的全局注册
const install = Vue => {
  components.forEach(component => {
    Vue.component(component.name, component);
  });

  // 注册 element-ui 提供的 v-loading 指令
  Vue.use(Loading.directive);
  Vue.prototype.$loading = Loading.service;
};

/* istanbul ignore if */
if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue);
}
export default install;
