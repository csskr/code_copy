import { DASHBOARD_ICONS, DASHBOARD_CHART_ICONS } from '@/constate/theme';
import { ROOT_VALUE } from '@/constate/style';

const px2vwvh = ({ x, y, width, height, minHeight, minWidth }, scaleY) => {
  const BaseX = 1920;
  const BaseY = 1080;

  const style = {};
  const scaleTop = 1 - scaleY;
  const heightVh = height / BaseY * 100;
  let topVh = (y / BaseY * 100) - (heightVh * scaleTop);
  topVh = topVh >= 0 ? topVh : 0;
  style.left = `${x / BaseX * 100}vw`;
  style.top = `${topVh}vh`;
  style.width = `${width / BaseX * 100}vw`;
  style.height = `${heightVh * scaleY}vh`;
  style.position = 'absolute';

  if (minHeight && minWidth) {
    style.minHeight = `${minHeight / BaseY * 100}vh`;
    style.minWidth = `${minWidth / BaseX * 100}vw`;
  }

  return style;
};

export default {
  name: 'YunWidget',

  props: {
    // 背景框类型
    type: {
      type: String, // [state, chart, text, table]
      required: true,
    },
    float: { // 是否使用定位布局
      type: Boolean,
      default: true,
    },
    // 布局信息，需要处理成 rem
    layout: {
      type: Object,
      required: true,
      default: {
        x: 0,
        y: 0,
        width: 0,
        height: 0,
      },
    },
    // 是否显示筛选项图标
    showFilter: {
      type: Boolean,
      default: true,
    },
    // 是否显示设置图标
    showSetting: {
      type: Boolean,
      default: false,
    },
    // 标题
    title: String,
    // 图表类型
    chartType: Number,
    // 图标
    icon: String,
    // 组件Y轴缩放比例
    scaleY: Number,
  },

  methods: {
    handleSetting() {
      this.$emit('setting');
    },
    handleFilter() {
      this.$emit('filter');
    },
  },

  computed: {},

  render(h) {
    // 处理 type 用于标题图标的展示
    const types = ['state', 'chart', 'text', 'table'];
    const type = types.indexOf(this.type);

    if (type < 0) {
      return null;
    }

    // 标题图标
    const icons = this.icon
      ? [`${this.icon}`]
      : [
        `${DASHBOARD_ICONS[type] || ''}`,
        `${(this.type === 'chart' && DASHBOARD_CHART_ICONS[this.chartType - 1]) || ''}`,
      ];

    const positions = [
      'left',
      'right',
      'top',
      'bottom',
      'left-top',
      'left-bottom',
      'right-top',
      'right-bottom',
      'left-middle',
      'right-middle',
    ];

    const style = px2vwvh(this.layout || {}, this.scaleY || 1, ROOT_VALUE);

    return h(
      'div',
      {
        class: ['yun-widget'],
        style: this.float ? style : {},
      },
      [
        h(
          'div',
          {
            class: ['yun-widget_bg', `yun-widget_bg-${this.type}`],
          },
          positions.map(position => {
            return h('div', { class: `bg-${position}` });
          }),
        ),
        h(
          'div',
          {
            class: 'yun-widget_action',
          },
          [
            this.showSetting
              ? h('i', {
                class: 'iconfont icon-setting',
                on: {
                  click: this.handleSetting,
                },
              })
              : null,
            this.showFilter
              ? h('i', {
                class: 'iconfont icon-filter',
                on: {
                  click: this.handleFilter,
                },
              })
              : null,
          ],
        ),
        h(
          'div',
          {
            class: 'yun-widget_content',
          },
          [
            this.title &&
              h(
                'div',
                {
                  class: 'yun-widget_content-title',
                },
                [
                  h('i', { class: `iconfont icon-${icons.join('')}` }),
                  h('span', { class: 'yun-widget_content-title-text' }, this.title),
                ],
              ),
            h(
              'div',
              {
                class: 'yun-widget_content-warpper',
              },
              this.$slots.default,
            ),
          ],
        ),
      ],
    );
  },
};
