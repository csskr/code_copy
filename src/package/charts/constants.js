export const DEFAULT_THEME = {
  categoryAxis: {
    axisLine: { show: false },
    splitLine: { show: false },
    splitArea: { show: false },
    axisTick: {
      alignWithLabel: true
    },
  },
  valueAxis: {
    axisLine: { show: false },
    splitNumber: 4,
    splitLine: {
      lineStyle: {
        type: 'dashed'
      }
    },
    minInterval: 1
  },
  legend: {
    show: true,
    icon: 'roundRect',
    orient: 'horizontal', // 图例排布方向：vertical | horizontal
    itemWidth: 12, // 设置宽度
    itemHeight: 12, // 设置高度
    right: 0, // 距离容器左侧距离，可为百分比，也可以是 left、center、right
    itemGap: 20,
    textStyle: {
      color: '#fff',
      fontSize: 12,
      fontWeight: 200,
    }
  },
  line: {
    smooth: true,
  },
  grid: {
    containLabel: true,
    left: 10,
    right: 10,
  },
};

export const ECHARTS_SETTINGS = [
  'grid',
  'dataZoom',
  'visualMap',
  'toolbox',
  'title',
  'legend',
  'xAxis',
  'yAxis',
  'radar',
  'tooltip',
  'axisPointer',
  'brush',
  'geo',
  'timeline',
  'graphic',
  'series',
  'backgroundColor',
  'textStyle',
];
