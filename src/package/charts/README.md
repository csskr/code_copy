# YunChart 图表组件说明

### 一、组件设计

在 `/src/package/charts/core.js` 中实现核心代码，包括画布的初始化、`resize` 事件的自适应、数据变更时更新图表。然后基于 `core.js` 封装各个图表的子类，处理不同类型图表的数据、坐标轴。最后基于图表子类封装业务组件，实现定制化图表。

组件基于 **E-charts** 封装，将原本繁琐的配置项分类，由 `columns`、`rows` 提供图表数据，`settings` 添加具体的配置信息。为了保证视觉效果的一致性，启用了 `theme` 来配置大部分通用配置，因此 `settings` 实际并不需要传入太多的配置信息。



### 二、基础属性

| 配置项         | 简介                                                         | 类型            | 默认值 |
| -------------- | ------------------------------------------------------------ | --------------- | ------ |
| data           | 数据，包含 `columns` 和 `rows`                               | Object          | -      |
| settings       | 配置项，不同类型的图表需要的配置项不同                       | Object          | -      |
| initOptions    | charts.init() 的附加参数,  [参考文档](https://www.echartsjs.com/zh/api.html#echarts.init) | Object          | -      |
| width          | 宽度                                                         | String          | auto   |
| height         | 高度                                                         | String          | 100%   |
| colors         | 色盘                                                         | Array           | -      |
| theme          | 自定义主题，用于配置通用样式                                 | Object          | -      |
| echartsOptions | echarts 的原生配置，可覆盖 options                           | Object          | -      |
| setOptionOpts  | echarts setOption 的第二个参数,  [参考文档](http://echarts.baidu.com/api.html#echartsInstance.setOption) | Boolean, Object | true   |
| defaultAction  | 设置默认高亮的函数，可以接受一个 echarts 实例作为参数        | Function        | -      |
| loading        | 是否正在加载                                                 | Boolean         | false  |
| dataEmpty      | 是否空数据                                                   | Boolean         | false  |
| resizeDelay    | resize 函数的防抖延时                                        | Number          | 200    |
| changeDelay    | 数据更新时的防抖延时                                         | Number          | 0      |



### 三、组件通用 settings 配置项

| 配置项      | 简介                                                         | 类型    | 备注                                                         |
| ----------- | ------------------------------------------------------------ | ------- | ------------------------------------------------------------ |
| fontColor   | 坐标轴字体颜色                                               | String  | 默认 `#fff`                                                  |
| axisColor   | 坐标轴分割线颜色                                             | String  | 默认 `#fff`                                                  |
| itemStyle   | 图表元素的样式，在处理 series 数据时混入                     | Object  |                                                              |
| legend      | 图例样式, [参考文档](https://www.echartsjs.com/zh/option.html#legend) | Object  |                                                              |
| tooltip     | 悬浮窗样式, [参考文档](https://www.echartsjs.com/zh/option.html#tooltip) | Object  |                                                              |
| scale       | 设置为 `true` 之后，数据轴的坐标不会强制包涵零刻度。设置 min、max 之后无效。 | Array   | 由于需要兼容双轴的情况，故传入布尔值数组。<br>默认 `[false, false]` |
| min         | 坐标轴刻度最小值                                             | Array   | 同上。默认 `[null, null]`                                    |
| max         | 坐标轴刻度最大值                                             | Array   | 同上。默认 `[null, null]`                                    |
| axisVisible | 是否显示坐标轴，仅在有坐标轴的组件中生效                     | Boolean | true                                                         |
| xAxisOpt    | x 轴配置项，会覆盖默认配置，xAxis [参考文档](https://www.echartsjs.com/zh/option.html#xAxis) | Object  | 只可配置第一级配置，且会**直接覆盖，不会合并**               |
| yAxisOpt    | y 轴配置项，会覆盖默认配置，yAxis [参考文档](https://www.echartsjs.com/zh/option.html#yAxis) | Object  | 同上                                                         |

