import { copyByJson, isArray } from '@/util/util';

const pieRadius = 100;
const ringRadius = [80, 100];
const pieCenter = ['50%', '50%'];

// 处理数据
function getPieSeries(args) {
  const { innerRows, center, radius, label, labelLine, hoverAnimation, itemStyle } = args;
  if (!isArray(innerRows)) return;

  let seriesBase = {
    type: 'pie',
    center,
    radius,
    hoverAnimation,
  };

  const series = [];
  const seriesItem = Object.assign({ data: [] }, seriesBase);

  seriesItem.data = innerRows.map(item => {
    if (typeof item.value === 'string') {
      item.value = parseFloat(item.value) || 0;
    }
    return item || 0;
  });

  if (label) seriesItem.label = label;
  if (labelLine) seriesItem.labelLine = labelLine;
  if (itemStyle) seriesItem.itemStyle = itemStyle;

  series.push(seriesItem);

  return series;
}

// 饼图
export const pie = (columns, rows, settings, extra, isRing = false) => {
  const innerRows = copyByJson(rows);
  const {
    radius = isRing ? ringRadius : pieRadius,
    center = pieCenter,
    hoverAnimation = true,
    itemStyle,
    pieLabel,
    pieLabelLine,
    tooltip = {},
    legend = {},
    lengendSize,
  } = settings;

  const series = getPieSeries({
    innerRows,
    radius,
    center,
    label: pieLabel,
    labelLine: pieLabelLine,
    itemStyle,
    hoverAnimation,
  });

  const _legend = {
    formatter: name => {
      // 图例显示百分比
      if (!isArray(innerRows)) return;

      // lengendSize不属于echarts原生属性，这里用来判断name超出size后显示...
      if (lengendSize) {
        name = (name.length > lengendSize ? (name.slice(0, lengendSize) + '…') : name);
      }

      const percentInfo = innerRows.slice(0).reduce(
        (pre, item) => {
          if (item.name === name) {
            pre.currentValue = item.value;
          }
          pre.sum += item.value;
          return pre;
        },
        {
          sum: 0,
          currentValue: 0,
        },
      );
      const perent = `${Number(
        ((percentInfo.currentValue / percentInfo.sum) * 100).toFixed(2),
      )}%`;
      return `${name}: ${perent}`;
    },
    ...legend,
  };

  const options = { series, tooltip, legend: _legend };

  return options;
};

// 环图
export const ring = (columns, rows, settings, extra) => {
  return pie(columns, rows, settings, extra, true);
};
