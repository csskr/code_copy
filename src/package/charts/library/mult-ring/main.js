import { copyByJson, isArray, getArrayValue } from '@/util/util';

const pieRadius = 100;
const ringRadius = [80, 100];
const pieCenter = ['50%', '50%'];

function getRadius(i, width, space = []) {
  return [i * width + space[0] + '%', i * width + space[0] + space[1] + '%'];
}

function getYAxis(args) {
  const { axis, options } = args;
  const baseOpt = {
    type: 'category',
    inverse: true,
    axisLine: {
      show: false,
    },
    axisTick: {
      show: false,
    },
  };
  return [
    {
      ...baseOpt,
      axisLabel: {
        interval: 0,
        inside: true,
        textStyle: {
          color: '#fff',
          fontSize: 12,
        },
        show: true,
        ...options.axisLabel,
      },
      data: axis,
    },
  ];
}

// 处理数据
function getPieSeries(args) {
  const {
    sumValue,
    innerRows,
    center,
    hoverAnimation,
    itemStyle,
    bgColor,
    ringSpace,
    ringWidth,
  } = args;
  if (!isArray(innerRows)) return;

  let seriesBase = {
    type: 'pie',
    clockWise: false, // 顺时加载
    hoverAnimation,
    center,
    label: {
      show: false,
    },
  };
  if (itemStyle) seriesBase.itemStyle = itemStyle;

  const res = {
    series: [],
    axis: [],
    legendData: [],
  };

  innerRows.map((item, i) => {
    if (typeof item.value === 'string') {
      item.value = parseFloat(item.value) || 0;
    }
    // 主体
    res.series.push({
      ...seriesBase,
      radius: getRadius(i, ringWidth, ringSpace),
      data: [
        {
          value: item.value,
          name: item.name,
        },
        {
          value: sumValue - item.value,
          name: '',
          itemStyle: {
            color: 'transparent',
            borderWidth: 0,
          },
          tooltip: {
            show: false,
          },
        },
        /* sumValue * 0.33 为后 1/4 透明圆环，开启后会显得进度条过短，故暂时隐藏 */
        // {
        //   value: sumValue * 0.33,
        //   name: '',
        //   itemStyle: {
        //     color: 'transparent',
        //     borderWidth: 0,
        //   },
        //   tooltip: {
        //     show: false,
        //   },
        // },
      ],
    });
    // 背景环
    res.series.push({
      ...seriesBase,
      silent: true,
      z: 1,
      radius: getRadius(i, ringWidth, ringSpace),
      data: [
        {
          value: 7.5,
          itemStyle: {
            color: bgColor,
            borderWidth: 0,
          },
          tooltip: {
            show: false,
          },
        },
        {
          value: 2.5,
          name: '',
          itemStyle: {
            color: 'transparent',
            borderWidth: 0,
          },
          tooltip: {
            show: false,
          },
          hoverAnimation: false,
        },
      ],
    });
    // 占比
    let percent = (item.value / sumValue) * 100;
    res.axis.push(percent ? `${percent.toFixed(2)}%` : '0%');
    res.legendData.push(item.name);
  });

  // 坐标为从上到下，但圆环为从内到外，故倒序
  res.axis = res.axis.reverse();
  res.legendData = res.legendData.reverse();

  return res;
}

// 多环图
export const pie = (columns, rows, settings, extra, isRing = false) => {
  if (!isArray(rows)) {
    return;
  }
  const innerRows = copyByJson(rows);
  const {
    radius = isRing ? ringRadius : pieRadius,
    center = pieCenter,
    hoverAnimation = true,
    itemStyle,
    tooltip = {},
    legend = {},
    ringWidth = 20, // 环的粗细
    ringSpace = [20, 10], // 环的间距
    yAxisOpt = {}, // y轴配置项
    bgColor = 'rgba(25,88,173,0.40)',
  } = settings;

  const arrValue = getArrayValue(innerRows, 'value') || [];
  const sumValue =
    arrValue.length > 0 &&
    arrValue.reduce((prev, cur) => {
      return Number(prev) + Number(cur);
    });

  const { series, axis, legendData } = getPieSeries({
    sumValue,
    innerRows,
    radius,
    center,
    itemStyle,
    hoverAnimation,
    bgColor,
    ringWidth,
    ringSpace,
  });

  const yAxis = getYAxis({
    axis,
    options: yAxisOpt || {},
  });

  const xAxis = [{ show: false }];

  const options = {
    series,
    tooltip,
    legend: {
      orient: 'vertical',
      align: 'left',
      data: legendData,
      ...legend,
    },
    yAxis,
    xAxis,
  };

  return options;
};
