import { copyByJson, isArray } from '@/util/util';

// 处理维度坐标轴
function getDimAxis(args) {
  const { columns, dimAxisOpt, axisVisible, fontColor, axisColor } = args;
  return {
    type: 'category', // 类目轴
    name: dimAxisOpt.name,
    nameLocation: 'end', // 坐标轴名称显示位置 -> start / middle / center
    nameGap: 22, // 坐标轴名称与轴线之间的距离
    data: isArray(columns) ? columns : [],
    axisLabel: {
      color: fontColor,
      ...dimAxisOpt.axisLabel,
    },
    axisLine: {
      lineStyle: {
        width: 1,
        color: axisColor,
      },
      ...dimAxisOpt.axisLine,
    },
    show: axisVisible,
  };
}

// 处理数据轴
function getValAxis(args) {
  const { valAxisOpt, axisVisible, axisColor, fontColor, scale, min, max } = args;

  const valAxisBase = {
    type: 'value',
    axisTick: {
      show: false,
    },
    splitLine: {
      lineStyle: {
        color: axisColor,
      },
    },
    splitNumber: valAxisOpt.splitNumber || 5,
    axisLabel: {
      color: fontColor,
    },
    show: axisVisible,
  };

  const valAxis = [];
  // 双轴
  for (let i = 0; i < 2; i++) {
    valAxis[i] = Object.assign({}, valAxisBase);
    valAxis[i].name = (valAxisOpt.name && valAxisOpt.name[i]) || '';
    valAxis[i].scale = scale[i] || false;
    valAxis[i].min = min[i] || min[i] === 0 ? min[i] : null;
    valAxis[i].max = max[i] || null;
    valAxis[i].interval = (max[i] - min[i]) / valAxisOpt.splitNumber || null;
    if (valAxisOpt.axisLabel) {
      valAxis[i].axisLabel =
        valAxisOpt.axisLabel.length > 1
          ? {
            ...valAxisBase.axisLabel,
            ...valAxisOpt.axisLabel[i],
          }
          : {
            ...valAxisBase.axisLabel,
            ...(valAxisOpt.axisLabel[0] || valAxisOpt.axisLabel),
          };
    }
    if (valAxisOpt.axisLine) {
      valAxis[i].axisLine =
        valAxisOpt.axisLine.length > 1
          ? {
            ...valAxisOpt.axisLine[i],
          }
          : {
            ...(valAxisOpt.axisLine[0] || valAxisOpt.axisLine),
          };
    }
  }

  return valAxis;
}

// 处理数据
function getSeriesData(args) {
  const { innerRows, itemStyle } = args;
  if (!isArray(innerRows)) return;

  return innerRows.map(row => {
    row.type = row.type || 'bar';
    // row.yAxisIndex = index;
    row.data =
      row.data &&
      row.data.map(item => {
        if (typeof item === 'string') {
          return parseFloat(item) || 0;
        }
        return Number(item && item.toFixed(2)) || 0;
      });
    return Object.assign({}, row, itemStyle);
  });
}

// 双轴图（直方图&折线图）
export const biaxial = (columns, rows, settings, extra) => {
  // 如果 columns 为空，则不渲染图表
  if (!isArray(columns) || columns.length < 1) {
    return {};
  }
  const innerRows = copyByJson(rows);
  const {
    fontColor = '#fff', // 坐标轴字体颜色
    axisColor = '#fff', // 坐标系线条颜色
    itemStyle = {}, // 每个 dataItem 的样式
    legend = {}, // 图例样式
    tooltip = {}, // 提示框样式
    scale = [false, false],
    min = [null, null],
    max = [null, null],
    axisVisible = true,
    xAxisOpt = {}, // x轴配置项
    yAxisOpt = {}, // y轴配置项
  } = settings;

  const xAxis = getDimAxis({
    columns,
    dimAxisOpt: xAxisOpt,
    axisVisible,
    fontColor,
    axisColor,
  });

  const yAxis = getValAxis({
    valAxisOpt: yAxisOpt,
    axisVisible,
    axisColor,
    fontColor,
    scale,
    min,
    max,
  });

  const series = getSeriesData({
    innerRows,
    itemStyle,
  });

  const options = { yAxis, series, xAxis, legend, tooltip };

  return options;
};
