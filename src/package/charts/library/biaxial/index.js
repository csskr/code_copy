import 'echarts/lib/chart/bar';
import 'echarts/lib/chart/line';
import { biaxial } from './main';
import Core from '../../core';
export default Object.assign({}, Core, {
  name: 'ChartBiaxial',
  data() {
    this.chartHandler = biaxial;
    return {};
  },
});
