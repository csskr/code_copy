import { copyByJson, isArray } from '@/util/util';

// 处理维度坐标轴
function getBarDimAxis(args) {
  const { columns, dimAxisOpt, axisVisible, fontColor, axisColor } = args;
  return {
    type: 'category', // 类目轴
    name: dimAxisOpt.name,
    nameLocation: 'end', // 坐标轴名称显示位置 -> start / middle / center
    nameGap: 22, // 坐标轴名称与轴线之间的距离
    data: isArray(columns) ? columns : [],
    axisLabel: {
      color: fontColor,
      ...dimAxisOpt.axisLabel,
    },
    axisLine: {
      lineStyle: {
        width: 1,
        color: axisColor,
      },
      ...dimAxisOpt.axisLine,
    },
    show: axisVisible,
  };
}

// 处理数据轴
function getBarValAxis(args) {
  const { valAxisOpt, axisVisible, axisColor, fontColor, scale, min, max } = args;

  const valAxisBase = {
    type: 'value',
    axisTick: {
      show: false,
    },
    splitLine: {
      lineStyle: {
        color: axisColor,
      },
    },
    axisLabel: {
      color: fontColor,
      ...valAxisOpt.axisLabel,
    },
    axisLine: {
      ...valAxisOpt.axisLine,
    },
    show: axisVisible,
  };

  const valAxis = [];
  // 数据轴可以为双轴（未实现，故 i < 1）
  for (let i = 0; i < 1; i++) {
    valAxis[i] = Object.assign({}, valAxisBase);
    valAxis[i].name = (valAxisOpt.name && valAxisOpt.name[i]) || '';
    valAxis[i].scale = scale[i] || false;
    valAxis[i].min = min[i] || null;
    valAxis[i].max = max[i] || null;
  }

  return valAxis;
}

// 处理数据
function getBarSeries(args) {
  const { innerRows, itemStyle } = args;
  if (!isArray(innerRows)) return;

  return innerRows.map(row => {
    row.type = 'bar';
    row.data =
      row.data &&
      row.data.map(item => {
        if (typeof item === 'string') {
          return parseFloat(item) || 0;
        }
        return (item && item.toFixed(2)) || 0;
      });
    return Object.assign({}, row, itemStyle);
  });
}

// 条形图
export const bar = (columns, rows, settings, extra) => {
  // 如果 columns 为空，则不渲染图表
  if (!isArray(columns) || columns.length < 1) {
    return {};
  }
  const innerRows = copyByJson(rows);
  const {
    fontColor = '#fff', // 坐标轴字体颜色
    axisColor = '#fff', // 坐标系线条颜色
    itemStyle = {}, // 每个直方的样式
    legend = {}, // 图例样式
    tooltip = {}, // 提示框样式
    scale = [false, false],
    min = [null, null],
    max = [null, null],
    axisVisible = true,
    xAxisOpt = {}, // x轴配置项
    yAxisOpt = {}, // y轴配置项
  } = settings;

  const yAxis = getBarDimAxis({
    columns,
    axisVisible,
    dimAxisOpt: yAxisOpt,
    fontColor,
    axisColor,
  });

  const xAxis = getBarValAxis({
    axisVisible,
    valAxisOpt: xAxisOpt,
    axisColor,
    fontColor,
    scale,
    min,
    max,
  });

  const series = getBarSeries({
    innerRows,
    itemStyle,
  });

  const options = { yAxis, series, xAxis, legend, tooltip };

  return options;
};

// 直方图
export const histogram = (columns, rows, settings, extra) => {
  // 如果 columns 为空，则不渲染图表
  if (!isArray(columns) || columns.length < 1) {
    return {};
  }
  const innerRows = copyByJson(rows);
  const {
    fontColor = '#fff', // 坐标轴字体颜色
    axisColor = '#fff', // 坐标系线条颜色
    itemStyle = {}, // 每个直方的样式
    legend = {}, // 图例样式
    tooltip = {}, // 提示框样式
    scale = [false, false],
    min = [null, null],
    max = [null, null],
    axisVisible = true,
    xAxisOpt = {}, // x轴配置项
    yAxisOpt = {}, // y轴配置项
  } = settings;

  const xAxis = getBarDimAxis({
    columns,
    dimAxisOpt: xAxisOpt,
    axisVisible,
    fontColor,
    axisColor,
  });

  const yAxis = getBarValAxis({
    valAxisOpt: yAxisOpt,
    axisVisible,
    axisColor,
    fontColor,
    scale,
    min,
    max,
  });

  const series = getBarSeries({
    innerRows,
    itemStyle,
  });

  const options = { yAxis, series, xAxis, legend, tooltip };

  return options;
};
