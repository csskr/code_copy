import echarts from 'echarts/lib/echarts';
import { isArray } from '@/util/util';
import { getMapJSON } from '../../util';

// 处理数据
function getSeries(args) {
  const {
    columns,
    position,
    selectData,
    selectedMode,
    itemStyle,
    rows,
    roam,
    center,
    mapGrid,
  } = args;

  if (!isArray(columns) || !isArray(rows)) {
    return;
  }

  const result = [];
  const mapBase = {
    type: 'map',
    mapType: position,
  };

  columns.forEach(name => {
    const itemResult = {
      name,
      data: [],
      selectedMode,
      roam,
      center,
      ...mapBase,
    };

    if (mapGrid) {
      Object.keys(mapGrid).forEach(key => {
        itemResult[key] = mapGrid[key];
      });
    }
    setGeoLabel(itemStyle, itemResult, 'itemStyle');
    // setGeoLabel(label, itemResult, 'label');

    rows.forEach(row => {
      itemResult.data.push({
        name,
        value: row[name],
        selected: selectData,
      });
    });
    result.push(itemResult);
  });
}

function setGeoLabel(value, target, label) {
  if (typeof value === 'object') {
    target[label] = value;
  } else if (value) {
    target[label] = {
      normal: { show: true },
      emphasis: { show: true },
    };
  }
}

// 注册地图
function registerMap(args, mapOrigin) {
  const { _once, beforeRegisterMap, position, registerSign, specialAreas } = args;
  if (!_once[registerSign]) {
    if (beforeRegisterMap) {
      mapOrigin = beforeRegisterMap(mapOrigin);
    }
    _once[registerSign] = true;
    echarts.registerMap(position, mapOrigin, specialAreas);
  }
}

// 地图
export const map = (columns, rows, settings, extra) => {
  const { _once } = extra;
  const {
    legend = {}, // 图例样式
    tooltip = {}, // 提示框样式
    position = 'china',
    selectData = false, // 是否高亮显示数据对应位置
    selectedMode, // 地图选中模式
    itemStyle = {},
    center, // 当前视角中心
    roam, // 是否开启鼠标缩放和平移漫游
    mapGrid, // 地图距离容器边距
    mapURLProfix = 'https://unpkg.com/echarts@3.6.2/map/json/', // 位置请求的 URL 前缀
    positionJsonLink, // 地图数据源
    beforeRegisterMap, // 地图数据注册前执行的函数，参数为地图数据，需返回地图数据
    specialAreas, // 配置特定区域(https://www.echartsjs.com/zh/api.html#echarts.registerMap)
  } = settings;
  // geojson 文件
  let mapOrigin = settings.mapOrigin;
  // 地图数据
  const seriesParams = {
    columns,
    position,
    selectData,
    selectedMode,
    itemStyle,
    rows,
    roam,
    center,
    mapGrid,
  };
  const series = getSeries(seriesParams);

  const registerOptions = {
    _once,
    beforeRegisterMap,
    registerSign: `REGISTE_MAP_${position}`,
    position,
    specialAreas,
  };
  if (mapOrigin) {
    registerMap(registerOptions, mapOrigin);
    return {
      series,
      tooltip,
      legend,
    };
  } else {
    return getMapJSON({
      position,
      positionJsonLink,
      mapURLProfix,
    }).then(json => {
      registerMap(registerOptions, json);
      return { series, tooltip, legend };
    });
  }
};
