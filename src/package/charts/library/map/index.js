import 'echarts/lib/chart/map';
import 'echarts/lib/chart/effectScatter';
import 'echarts/lib/chart/scatter';
import 'echarts/lib/component/geo';
import { map } from './main';
import Core from '../../core';
export default Object.assign({}, Core, {
  name: 'ChartMap',
  data() {
    this.chartHandler = map;
    return {};
  },
});
