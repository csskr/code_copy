import 'echarts/lib/chart/pie';
import { area } from '../area-ring/main';
import Core from '../../core';
export default Object.assign({}, Core, {
  name: 'ChartAreaRing',
  data() {
    this.chartHandler = area;
    return {};
  },
});
