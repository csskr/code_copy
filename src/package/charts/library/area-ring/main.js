import { copyByJson, isArray } from '@/util/util';

const ringRadius = [100, 130];
const ringCenter = ['50%', '50%'];

// 处理数据
function getSeries(args) {
  const {
    innerRows,
    center,
    hoverAnimation,
    radius,
    label,
    radiusSize,
    itemStyle,
    labelLine,
  } = args;

  let seriesBase = {
    type: 'pie',
    hoverAnimation,
    center,
  };

  const series1 = {
    ...seriesBase,
    radius,
    data: [
      {
        value: innerRows[0].value,
        name: innerRows[0].name,
        label: {
          show: false,
          ...label[0]
        },
        labelLine: {
          show: false,
          ...labelLine[0]
        },
        itemStyle: itemStyle[0],
      },
      {
        value: innerRows[1].value,
        name: innerRows[1].name,
        itemStyle: {
          color: 'transparent',
        },
      },
    ],
  };

  const series2 = {
    ...seriesBase,
    radius: [radius[0] + radiusSize, radius[1] - radiusSize],
    data: [
      {
        value: innerRows[0].value,
        name: innerRows[0].name,
        itemStyle: {
          color: 'transparent',
        },
      },
      {
        value: innerRows[1].value,
        name: innerRows[1].name,
        label: {
          show: false,
          ...label[1] || label[0]
        },
        labelLine: {
          show: false,
          ...labelLine[1] || labelLine[0]
        },
        itemStyle: itemStyle[1] || itemStyle[0],
      },
    ],
  };

  return [series1, series2];
}

// 用 title 展示百分比
function getTitle(args) {
  const { titleOpt, innerRows } = args;
  const text = Number(((innerRows[0].value / (innerRows[0].value + innerRows[1].value)) * 100).toFixed(1));
  return {
    show: true,
    text: `${text}%`,
    subtext: innerRows[0].name,
    x: 'center',
    y: 'center',
    textStyle: {
      color: '#fff',
      fontSize: 30,
      fontWeight: 'normal',
    },
    subtextStyle: {
      color: 'rgba(255,255,255,.45)',
      fontSize: 14,
      fontWeight: 'normal',
    },
    ...titleOpt,
  };
}

// 双环图 - 仅限两条数据
export const area = (columns, rows, settings, extra) => {
  if (!isArray(rows) || rows.length < 2) {
    return;
  }

  const _rows = rows.slice(0, 2).map(item => {
    return {
      ...item,
      value: Number(item.value),
    };
  });
  const innerRows = copyByJson(_rows);

  const {
    center = ringCenter,
    radius = ringRadius,
    radiusSize = 10, // 两个环之间的 radius 差值
    legend = {}, // 图例样式
    tooltip = {}, // 提示框样式
    itemStyle = [], // 长度为2的数组，分别为第一块和第二块的样式
    labelLine = [], // 长度为2的数组，分别为第一块和第二块的指导线
    label = [], // 长度为2的数组
    titleOpt = {}, // title 配置项
    hoverAnimation = true,
  } = settings;

  const series = getSeries({
    innerRows,
    radius,
    radiusSize,
    center,
    itemStyle,
    label,
    labelLine,
    hoverAnimation,
  });

  const title = getTitle({ titleOpt, innerRows });

  const options = { series, tooltip, legend, title };

  return options;
};
