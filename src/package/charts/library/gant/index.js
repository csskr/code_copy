import 'echarts/lib/chart/custom';
import { gant } from './main';
import Core from '../../core';
export default Object.assign({}, Core, {
  name: 'ChartGant',
  data() {
    this.chartHandler = gant;
    return {};
  },
});
