import echarts from 'echarts/lib/echarts';
import { copyByJson, isArray } from '@/util/util';

const DIM_CATEGORY_INDEX = 0; // value 中类目标识的索引
const DIM_CATEGORY_NAME_INDEX = 1; // value 中对应元素类型的索引
const DIM_START_TIME_INDEX = 3; // value 中开始时间的索引
const DIM_END_TIME_INDEX = 4; // value 中结束时间的索引
const DIM_DURATION_INDEX = 5; // value 中持续时长的索引

const HEIGHT_RATIO = 0.6; // 甘特图矩形元素高度缩放比例
const CATEGORY_NAME_PADDING_WIDTH = 20; // 在甘特图矩形元素上展示文字时，左右 padding 的最小长度

// 处理维度坐标轴
function getGantDimAxis(args) {
  const { columns, fontColor, dimAxisOpt, axisColor, axisVisible } = args;
  const data = columns;
  return [
    {
      type: 'category',
      data,
      inverse: true,
      axisTick: {
        show: false,
        ...dimAxisOpt.axisTick,
      },
      axisLabel: {
        fontSize: 14,
        color: fontColor,
        ...dimAxisOpt.axisLabel,
      },
      axisLine: {
        lineStyle: {
          width: 1,
          color: axisColor,
        },
        ...dimAxisOpt.axisLine,
      },
      splitLine: {
        show: true,
        lineStyle: {
          color: axisColor,
        },
        ...dimAxisOpt.splitLine,
      },
      show: axisVisible,
    }
  ];
}

// 处理数据轴
function getGantValAxis(args) {
  const { axisColor, valAxisOpt, interval, axisVisible, fontColor, max, min } = args;
  return {
    type: 'time',
    position: 'top',
    interval,
    max: max[0],
    min: min[0],
    axisTick: {
      show: true,
      ...valAxisOpt.axisTick,
    },
    axisLine: {
      ...valAxisOpt.axisLine,
    },
    splitLine: {
      lineStyle: {
        color: axisColor,
      },
    },
    axisLabel: {
      color: fontColor,
      fontSize: 12,
      align: 'right',
      ...valAxisOpt.axisLabel,
    },
    show: axisVisible,
  };
}

// 处理数据
function getGantSeries(args) {
  const { innerRows, columns, barMaxHeight, barHeight, itemStyle, isShowText } = args;
  if (!isArray(innerRows)) return;
  /**
   * innerRows 的结构为按状态划分的二维数组，且元素必须为包含 name 和 value 的对象
   * 其中 value 为数组，其元素可以通过 api.value(index) 获取
   * value 的元素依序为：“对应 category 的索引”、“状态类型”、“状态名称”、“开始时间”、“结束时间”、“持续时长”
   * 示例：{ name: '', value: [] }
   */
  const baseItem = {
    type: 'custom',
    renderItem: (params, api) => renderGanttItem(params, api, {
      isShowText,
      barMaxHeight,
      barHeight,
    }),
    dimensions: columns,
    encode: {
      x: [DIM_START_TIME_INDEX, DIM_END_TIME_INDEX],
      y: DIM_CATEGORY_INDEX,
      seriesName: DIM_CATEGORY_NAME_INDEX,
      tooltip: [DIM_START_TIME_INDEX, DIM_END_TIME_INDEX, DIM_DURATION_INDEX],
    },
    animationDurationUpdate: 500,
  };
  return innerRows.map(row => {
    return {
      ...baseItem,
      name: row[0].name,
      itemStyle: Object.assign({}, itemStyle, row[0].itemStyle),
      data: row,
    };
  });
}

// 渲染甘特图元素
function renderGanttItem(params, api, extra) {
  const { isShowText, barMaxHeight, barHeight } = extra;
  // 使用 api.value(index) 取出当前 dataItem 的维度
  const categoryIndex = api.value(DIM_CATEGORY_INDEX);
  // 使用 api.coord(...) 将数值在当前坐标系中转换成为屏幕上的点的像素值
  const startPoint = api.coord([api.value(DIM_START_TIME_INDEX), categoryIndex]);
  const endPoint = api.coord([api.value(DIM_END_TIME_INDEX), categoryIndex]);
  // 使用 api.size(...) 取得坐标系上一段数值范围对应的长度
  const baseHeight = Math.min(api.size([0, 1])[1], barMaxHeight);
  const height = barHeight * HEIGHT_RATIO || baseHeight * HEIGHT_RATIO;
  const width = endPoint[0] - startPoint[0];
  const x = startPoint[0];
  const y = startPoint[1] - height / 2;

  // 处理类目名，用于在图形上展示
  const categoryName = api.value(DIM_CATEGORY_NAME_INDEX) + '';
  const categoryNameWidth = echarts.format.getTextRect(categoryName).width;
  const text = width > categoryNameWidth + CATEGORY_NAME_PADDING_WIDTH ? categoryName : '';

  const rectNormal = clipRectByRect(params, {
    x,
    y,
    width,
    height,
  });
  const rectText = clipRectByRect(params, {
    x,
    y,
    width,
    height,
  });

  return {
    type: 'group',
    children: [
      {
        // 图形元素形状: 'rect', circle', 'sector', 'polygon'
        type: 'rect',
        ignore: !rectNormal, // 是否忽略（忽略即不渲染）
        shape: rectNormal,
        // 映射 option 中 itemStyle 样式
        style: api.style(),
      },
      {
        // 在图形上展示类目名
        type: 'rect',
        ignore: !isShowText || !rectText,
        shape: rectText,
        style: api.style({
          fill: 'transparent',
          stroke: 'transparent',
          text: text,
          textFill: '#fff',
        }),
      },
    ],
  };
}

/**
 * 计算元素位置及宽高
 * 如果元素超出了当前坐标系的包围盒，则剪裁这个元素
 * 如果元素完全被剪掉，会返回 undefined
 */
function clipRectByRect(params, rect) {
  return echarts.graphic.clipRectByRect(rect, {
    x: params.coordSys.x,
    y: params.coordSys.y,
    width: params.coordSys.width,
    height: params.coordSys.height,
  });
}

// 甘特图
export const gant = (columns, rows, settings, extra) => {
  // 如果 columns 为空，则不渲染图表
  if (!isArray(columns) || columns.length < 1) {
    return {};
  }
  const innerRows = copyByJson(rows);
  const {
    fontColor = '#fff', // 坐标轴字体颜色
    axisColor = '#fff', // 坐标系线条颜色
    itemStyle = {}, // 每个直方的样式
    scale = [false, false],
    min = [null],
    max = [null],
    barHeight, // 矩形元素的固定高度
    barMaxHeight, // 矩形元素的最大高度，设置 barHeight 时失效
    axisVisible = true,
    interval = null,
    isShowText = false, // 是否在元素上显示名称
    xAxisOpt = {}, // x轴配置项
    yAxisOpt = {}, // y轴配置项
  } = settings;

  const yAxis = getGantDimAxis({
    columns,
    dimAxisOpt: yAxisOpt,
    axisVisible,
    fontColor,
    axisColor,
  });

  const xAxis = getGantValAxis({
    axisVisible,
    valAxisOpt: xAxisOpt,
    axisColor,
    fontColor,
    interval,
    scale,
    min,
    max,
  });

  const series = getGantSeries({
    innerRows,
    itemStyle,
    barMaxHeight,
    barHeight,
    columns,
    isShowText,
  });

  const options = {
    yAxis,
    series,
    xAxis,
  };

  return options;
};
