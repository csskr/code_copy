/**
 * 图表组件库工具函数
 */

export const $get = url => {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', url);
    xhr.send(null);
    xhr.onload = () => {
      resolve(JSON.parse(xhr.responseText));
    };
    xhr.onerror = () => {
      reject(JSON.parse(xhr.responseText));
    };
  });
};

const mapPromise = {};

// export const getMapJSON = ({ position, positionJsonLink, beforeRegisterMapOnce, mapURLProfix }) => {
//   const link = positionJsonLink || `${mapURLProfix}${position}.json`;
//   if (!mapPromise[link]) {
//     mapPromise[link] = $get(link).then(res => {
//       if (beforeRegisterMapOnce) res = beforeRegisterMapOnce(res);
//       return res;
//     });
//   }
//   return mapPromise[link];
// };
