import ChartBar from './library/bar';
import ChartHistogram from './library/histogram';
import ChartLine from './library/line';
import ChartPie from './library/pie';
import ChartRing from './library/ring';
import ChartGant from './library/gant';
import ChartBiaxial from './library/biaxial';
import ChartMap from './library/map';
import ChartMultRing from './library/mult-ring';
import ChartAreaRing from './library/area-ring';

export default [
  ChartPie,
  ChartRing,
  ChartBar,
  ChartHistogram,
  ChartLine,
  ChartGant,
  ChartBiaxial,
  ChartMultRing,
  ChartMap,
  ChartAreaRing,
];
