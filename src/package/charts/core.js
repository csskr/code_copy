import echartsLib from 'echarts/lib/echarts';
import 'echarts/lib/component/tooltip';
import 'echarts/lib/component/legend';
import 'echarts/lib/component/title';
import 'echarts/lib/component/dataZoom';

import { camelToKebab, debounce } from '../../util/util';

import Loading from './components/loading';
import DataEmpty from './components/empty';

import { DEFAULT_THEME, ECHARTS_SETTINGS } from './constants';

export default {
  props: {
    // 图表数据，{ columns: [], rows: [] }
    data: {
      type: [Object, Array],
      default() {
        return {};
      },
    },
    // echarts.init() 的附加参数
    initOptions: {
      type: Object,
      default() {
        return {};
      },
    },
    width: {
      type: String,
      default: 'auto',
    },
    height: {
      type: String,
      default: '100%',
    },
    colors: {
      type: Array,
    },
    // 自定义主题
    theme: Object,
    // 图表配置项
    settings: {
      type: Object,
      default() {
        return {};
      },
    },
    // echarts 的原生配置，可覆盖 options
    echartsOptions: {
      type: Object,
    },
    // 窗口 resize 事件回调的延迟
    resizeDelay: {
      type: Number,
      default: 200,
    },
    // 数据变更后重绘图表的延时
    changeDelay: {
      type: Number,
      default: 0,
    },
    // 是否加载
    loading: Boolean,
    // 是否空数据
    dataEmpty: Boolean,
    // 设置图表实例的配置项，为 Boolean 时表示 notMerge
    setOptionOpts: {
      type: [Boolean, Object],
      default: true,
    },
    // 默认高亮函数
    defaultAction: {
      type: Function
    }
  },

  watch: {
    data: {
      deep: true,
      handler(v) {
        if (v) {
          this.changeHandler();
        }
      },
    },
    settings: {
      deep: true,
      handler(v) {
        if (v && v.type && this.chartLib) {
          this.chartHandler = this.chartLib[v.type];
        }
        this.changeHandler();
      },
    },
    echartsOptions: {
      deep: true,
      handler(v) {
        if (v && v.type && this.chartLib) {
          this.chartHandler = this.chartLib[v.type];
        }
        this.changeHandler();
      },
    },
    width: 'nextTickResize',
    height: 'nextTickResize',
  },

  computed: {
    // 画布样式
    canvasStyle() {
      return {
        width: this.width,
        height: this.height,
        position: 'relative',
      };
    },
  },

  created() {
    this.echarts = null;
    this._once = {};
    this.resizeHandler = debounce(this.resize, this.resizeDelay);
    this.changeHandler = debounce(this.dataHandler, this.changeDelay);
  },

  mounted() {
    this.init();
  },

  beforeDestroy() {
    this.clean();
  },

  methods: {
    // 初始化
    init() {
      if (this.echarts) return;

      const theme = this.theme || DEFAULT_THEME;
      this.echarts = echartsLib.init(this.$refs.canvas, theme, this.initOptions);
      if (this.data) {
        this.changeHandler();
      }
      this.addResizeListener();
    },

    // 处理数据
    dataHandler() {
      // 图表组件继承 core 的时候会赋值 chartHandler
      if (!this.chartHandler) return;
      const data = this.data || {};
      const { columns, rows } = data;
      // 图表个性化显示的补充配置
      const extra = {
        echarts: this.echarts,
        _once: this._once
      };

      const options = this.chartHandler(columns, rows, this.settings, extra) || {};
      if (this.colors) options.color = this.colors;
      if (options) {
        this.optionsHandler(options);
      }
    },

    // 整理配置项并生成图表
    optionsHandler(options) {
      // 与 echarts 配置项对应的属性可以直接覆盖 options 中对应属性
      ECHARTS_SETTINGS.forEach(setting => {
        if (this.echartsOptions && this.echartsOptions[setting]) {
          options[setting] = this.echartsOptions[setting];
        }
      });

      this.echarts.setOption(options, this.setOptionOpts);
      // 设置默认高亮
      if (this.defaultAction) this.defaultAction(this.echarts);
    },

    // 监听 resize 事件
    addResizeListener() {
      this.removeResizeListener();
      window.addEventListener('resize', this.resizeHandler);
    },

    removeResizeListener() {
      try {
        window.removeEventListener('resize', this.resizeHandler);
      } catch {}
    },

    nextTickResize() {
      this.$nextTick(this.resize);
    },

    echartsResize() {
      this.echarts && this.echarts.resize();
    },

    // 自适应宽高
    resize() {
      if (this.$el && this.$el.clientWidth && this.$el.clientHeight) {
        this.echartsResize();
      }
    },

    // 销毁图表
    clean() {
      this.removeResizeListener();
      this.echarts.dispose();
    },
  },

  render(h) {
    return h(
      'div',
      {
        class: [camelToKebab(this.$options.name || this.$options._componentTag)],
        style: this.canvasStyle,
      },
      [
        h('div', {
          style: this.canvasStyle,
          class: { 'yun-charts-mask-status': this.dataEmpty || this.loading },
          ref: 'canvas',
        }),
        h(DataEmpty, {
          style: { display: this.dataEmpty ? '' : 'none' },
        }),
        h(Loading, {
          style: { display: this.loading ? '' : 'none' },
        }),
        this.$slots.default,
      ],
    );
  },
};
