import Vue from 'vue';
import App from './App.vue';
import ElementUI from 'element-ui';
import moment from 'moment';
import router from './router';

import VCharts from 'v-charts';
import './assets/index.less';
import store from './store/index';
import Base from './package/index';
import 'element-ui/lib/theme-chalk/index.css';

// 引入全局错误收集
import errorHandler from '@/util/error';
// 动态设置font-size
import { setRemInit } from '@/util/rem';
setRemInit();
import stores from "./store";
import {
  post,
  get,
  patch,
  put
} from './axios'
Vue.prototype.$store = store;
Vue.prototype.$post = post;
Vue.prototype.$get = get;
Vue.prototype.$patch = patch;
Vue.prototype.$put = put;
Vue.prototype.$moment = moment;
// 捕获全局错误 仅限同步代码
Vue.config.errorHandler = errorHandler;
Vue.use(VCharts);
Vue.use(Base);
Vue.use(ElementUI);
Vue.config.devtools = true

Vue.config.productionTip = false

new Vue({
  stores,
  router,
  render: h => h(App),
}).$mount('#app')
