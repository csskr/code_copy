import Vue from 'vue';
import Router from 'vue-router';
Vue.use(Router);
const router = new Router({
    routes: [
        {
            path: '/',
            name: 'index',
            redirect: '/index',
            component: ()=>import('../index')
        },
        {
            path: '/',
            name: 'index',
            component: ()=>import('../index')
        },
        {
            path: '/GLMap',
            name: 'GLMap',
            component:  ()=>import('../components/GLMap')
        },
        {
            path: '/CTMap',
            name: 'CTMap',
            component: ()=>import('../components/CTMap')
        },
        {
            path: '/NewsEvent',
            name: 'NewsEvent',
            component: ()=>import('../components/NewsEvent')
        },
        {
            path: '/LivelyMedia',
            name: 'LivelyMedia',
            component: ()=>import('../components/LivelyMedia')
        },
        {
            path: '/MediaAnalysis',
            name: 'MediaAnalysis',
            component: ()=>import('../components/MediaAnalysis')
        },
        {
            path: '/EmergentEvent',
            name: 'EmergentEvent',
            component: ()=>import('../components/EmergentEvent')
        },
        {
            path: '/EmotionalTrend',
            name: 'EmotionalTrend',
            component: ()=>import('../components/EmotionalTrend')
        },
        {
            path: '/PublicSentimentHot',
            name: 'PublicSentimentHot',
            component: ()=>import('../components/PublicSentimentHot')
        },
        {
            path: '/EmotionsAnalysis',
            name: 'EmotionsAnalysis',
            component: ()=>import('../components/EmotionsAnalysis')
        }
    ]
});
export default router
// router.beforeEach( (to,from,next) => {
//     console.log(from.path,to,from,next)
//     // if(from.path  != '/'){
//     //     next({
//     //         path:'/index'
//     //     })
//     // }else{
//     //     next()
//     // }
//   })