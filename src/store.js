import Vue from "vue";
import Vuex from "vuex";
import getters from "@/vuex/getters"
import actions from "@/vuex/actions"
import mutations from "@/vuex/mutations"
// import VuexPersistence from 'vuex-persist'
Vue.use(Vuex);
// const vuexLocal = new VuexPersistence({
//     storage: window.localStorage
// })
const state={
    count: 0,
    lang:"cn",
    editableTabs:[],
    cityName:'长葛市'
}
const store = new Vuex.Store({
    state,
    getters,
    actions,
    mutations,
});
export default store;