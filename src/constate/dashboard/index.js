// 定义的五类图表类型的常量，需要和详情接口返回的字段一致，在 model 中会作为 key 存在
export const DIALOG_TYPE = {
  STATE: 'states',
  BUSINESS: 'buss',
  CHART: 'charts',
  TASK: 'tasks',
  TEXT: 'texts',
  GANTT: 'gantts',
};

// 业务组件类型参数字典（业务组件类型 1开关机 2点检 3保养 4检验管理 5异常管理）
export const BUSS_TYPE = {
  1: {
    type: 'onoff',
    name: 'status_name',
  },
  2: {
    type: 'check',
    name: 'check_status_name',
  },
  3: {
    type: 'care',
    name: 'care_status_name',
  },
  4: {
    type: 'qa_manager',
    name: 'qa_status_name'
  },
  5: {
    type: 'excep_manager',
    name: 'excep_status_name'
  }
};

/* 和客户端交互的变量名 */
export const WEBVIEW_VAR = {
  DASHBOARD_ID: 'dashboard_id', // 仪表盘id
  DASHBOARD_CONDITIONS: 'dashboard_conditions', // 仪表盘参数
  IS_DASHBOARD_GROUP: 'is_dashboard_group', // 是否为仪表盘组
};

// TV看板菜单方向键触发标志位
export const DIRECTION_FLAG = {
  TOP: -2,
  BOTTOM: 2,
  LEFT: -1,
  RIGHT: 1
};

// TV遥控器按键事件名
export const TV_EVENTS = {
  LEFT: 'dashboard_tv_left',
  RIGHT: 'dashboard_tv_right',
  BOTTOM: 'dashboard_tv_bottom',
  TOP: 'dashboard_tv_top',
  BACK: 'dashboard_tv_back',
  ENTER: 'dashboard_tv_enter',
};
