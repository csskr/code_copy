// 图表标题图标
export const DASHBOARD_ICONS = ['state', 'chart', 'file', 'table'];
export const DASHBOARD_CHART_ICONS = ['bar', 'line', 'pie', 'ibar', 'table', 'biaxial'];
