/* 全局错误收集 */
import moment from 'moment';
import store from '@/store/index';

export default (err, vm = {}, info, async) => {
  const errInfo = [
    {
      event: async ? 'warning' : 'error',
      detail: JSON.stringify({
        ...err,
        message: err.message
      }),
      vm: vm.$vnode && vm.$vnode.tag,
      page: window.location.href.replace(/http:\/\/|https:\/\//, ''),
      time: moment().format('YYYY-MM-DD HH:mm'),
      async,
    }
  ];
  console.warn('KanBanError====>', errInfo);
  store.mutations.SaveAppException(JSON.stringify(errInfo));
};
