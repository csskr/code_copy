/* eslint-disable */

const detail = {"message":"ok","code":"0000","data":{"id": "666","charts":[{"chart_columns":[{"hashcode":15888,"column_id":15666,"column_name":"及时率","use_column_name":"及时率"}],"chart_conditions":null,"chart_type":5,"default_org_ids":null,"default_time_range_type":2,"dimension_column_id":15244,"dimension_column_name":"人员","id":18666,"key":"1566791664666","name":"图表测试","order_by":null,"report_config_id":1793,"report_type":1,"sort_column_id":null,"style":null,"time_query_type":3,"top_n":10,"use_org_condition":null},{"chart_columns":[{"hashcode":15277,"column_id":15246,"column_name":"及时率","use_column_name":"及时率"}],"chart_conditions":[{"condition_id": 287,"condition_name": "使用部门","default_selected_values": [2017,2019,2021],"hashcode": 318,"use_condition_name": "使用部门"},{"condition_id": 288,"condition_name": "使用部门2","default_selected_values": [],"hashcode": 789,"use_condition_name": "使用部门2"}],"chart_type":1,"default_org_ids":null,"default_time_range_type":2,"dimension_column_id":15244,"dimension_column_name":"人员","id":18368,"key":"1566791664144","name":"人员响应及时率-柱状图","order_by":null,"report_config_id":1793,"report_type":1,"sort_column_id":null,"style":null,"time_query_type":3,"top_n":10,"use_org_condition":null},{"chart_columns":[{"hashcode":58349,"column_id":58318,"column_name":"设备利用率","use_column_name":"设备利用率"}],"chart_conditions":null,"chart_type":3,"default_org_ids":null,"default_time_range_type":2,"dimension_column_id":58312,"dimension_column_name":"设备名称","id":18369,"key":"1566797727297","name":"设备利用率-折线图","order_by":null,"report_config_id":6432,"report_type":1,"sort_column_id":null,"style":null,"time_query_type":3,"top_n":10,"use_org_condition":null},{"chart_columns":[{"hashcode":15261,"column_id":15230,"column_name":"故障率","use_column_name":"故障率"}],"chart_conditions":null,"chart_type":4,"default_org_ids":null,"default_time_range_type":2,"dimension_column_id":15224,"dimension_column_name":"设备名称","id":18370,"key":"1566797743023","name":"设备故障率-条形图","order_by":null,"report_config_id":1790,"report_type":1,"sort_column_id":null,"style":null,"time_query_type":3,"top_n":10,"use_org_condition":null},{"chart_columns":[{"hashcode":15253,"column_id":15222,"column_name":"故障率","use_column_name":"故障率"}],"chart_conditions":null,"chart_type":2,"default_org_ids":null,"default_time_range_type":2,"dimension_column_id":15220,"dimension_column_name":"部门","id":18371,"key":"1566797864690","name":"部门设备故障率-折线图","order_by":null,"report_config_id":1789,"report_type":1,"sort_column_id":null,"style":null,"time_query_type":3,"top_n":10,"use_org_condition":null},{"chart_columns":[{"hashcode":46611,"column_id":46580,"column_name":"点检次数","use_column_name":"点检次数"}],"chart_conditions":null,"chart_type":6,"default_org_ids":null,"default_time_range_type":3,"dimension_column_id":46570,"dimension_column_name":"物资名称","id":18372,"key":"1566797874552","name":"巡检记录-双轴图","order_by":null,"report_config_id":5124,"report_type":1,"sort_column_id":null,"style":null,"time_query_type":4,"top_n":10,"use_org_condition":null}],"lay_out":"{\"1566791222222\":{\"x\":880,\"y\":600,\"width\":300,\"height\":150},\"1566791333333\":{\"x\":1200,\"y\":600,\"width\":300,\"height\":150},\"1566791664666\":{\"x\":0,\"y\":1320,\"width\":500,\"height\":300},\"1566790906781\":{\"x\":650,\"y\":0,\"width\":300,\"height\":150},\"1566790906777\":{\"x\":960,\"y\":0,\"width\":320,\"height\":150},\"1566790970303\":{\"x\":9,\"y\":0,\"width\":300,\"height\":150},\"1566790988394\":{\"x\":9,\"y\":160,\"width\":300,\"height\":150},\"1566790994649\":{\"x\":330,\"y\":160,\"width\":300,\"height\":150},\"1566791027433\":{\"x\":1293,\"y\":141,\"width\":300,\"height\":150},\"1566791044279\":{\"x\":650,\"y\":160,\"width\":300,\"height\":150},\"1566791089636\":{\"x\":971,\"y\":160,\"width\":300,\"height\":150},\"1566791122009\":{\"x\":1291,\"y\":0,\"width\":300,\"height\":150},\"1566791349233\":{\"x\":330,\"y\":0,\"width\":300,\"height\":150},\"1566791372668\":{\"x\":1609,\"y\":141,\"width\":300,\"height\":150},\"1566791380132\":{\"x\":1610,\"y\":0,\"width\":300,\"height\":150},\"1566791664144\":{\"x\":9,\"y\":280,\"width\":360,\"height\":300},\"1566797727297\":{\"x\":390,\"y\":281,\"width\":370,\"height\":300},\"1566797743023\":{\"x\":780,\"y\":282,\"width\":370,\"height\":300},\"1566797864690\":{\"x\":1550,\"y\":281,\"width\":360,\"height\":300},\"1566797874552\":{\"x\":1171,\"y\":282,\"width\":360,\"height\":300},\"1566791693820\":{\"x\":21,\"y\":600,\"width\":730,\"height\":350},\"1566791745164\":{\"x\":792,\"y\":600,\"width\":1110,\"height\":350}}","name":"秋田模板2","operator":"王腾","operator_id":166,"states":[{"default_dimension_type":null,"default_start_time":null,"default_end_time":null,"default_time_query_type":1,"id":18373,"key":"1566790970303","name":"状态组件-tNfD","style":null,"status_id":"13584","status_name":"开机","default_org_ids":null},{"default_dimension_type":null,"default_start_time":null,"default_end_time":null,"default_time_query_type":1,"id":18374,"key":"1566790988394","name":"状态组件-CuqY","style":null,"status_id":"411","status_name":"待维修","default_org_ids":null},{"default_dimension_type":null,"default_start_time":null,"default_end_time":null,"default_time_query_type":1,"id":18375,"key":"1566790994649","name":"状态组件-x1np","style":null,"status_id":"415","status_name":"维修中","default_org_ids":null},{"default_dimension_type":null,"default_start_time":null,"default_end_time":null,"default_time_query_type":1,"id":18376,"key":"1566791027433","name":"状态组件-ge0j","style":null,"status_id":"417","status_name":"待再次维修","default_org_ids":null},{"default_dimension_type":null,"default_start_time":null,"default_end_time":null,"default_time_query_type":1,"id":18377,"key":"1566791044279","name":"状态组件-hCLb","style":null,"status_id":"416","status_name":"维修结束","default_org_ids":null},{"default_dimension_type":null,"default_start_time":null,"default_end_time":null,"default_time_query_type":1,"id":18378,"key":"1566791089636","name":"状态组件-zOtD","style":null,"status_id":"417","status_name":"待再次维修","default_org_ids":null},{"default_dimension_type":null,"default_start_time":null,"default_end_time":null,"default_time_query_type":1,"id":18379,"key":"1566791122009","name":"状态组件-KOOM","style":null,"status_id":"13710","status_name":"记录提交","default_org_ids":null},{"default_dimension_type":null,"default_start_time":null,"default_end_time":null,"default_time_query_type":2,"id":18380,"key":"1566791349233","name":"状态组件-jfqQ","style":null,"status_id":"13586","status_name":"关机","default_org_ids":null},{"default_dimension_type":null,"default_start_time":null,"default_end_time":null,"default_time_query_type":1,"id":18381,"key":"1566791372668","name":"状态组件-YviD","style":null,"status_id":"1167","status_name":"正常维修","default_org_ids":null},{"default_dimension_type":null,"default_start_time":null,"default_end_time":null,"default_time_query_type":1,"id":18382,"key":"1566791380132","name":"状态组件-4nxp","style":null,"status_id":"410","status_name":"等待抢单","default_org_ids":null}],"tasks":[{"biz_code":1,"default_dimension_type":null,"default_org_ids":null,"id":18383,"key":"1566791693820","name":"设备点巡检/保养实时任务表","states":[{"color":"#F5A623","status_id":"17226"},{"color":"#F5A623","status_id":"17242"}],"style":null,"task_columns":[{"column_name":"名称","expression":null,"fetch_method":"{\"fetch_column\":31,\"fetch_table\":3}","hashcode":-318965052,"use_column_name":"名称"},{"column_name":"编号","expression":null,"fetch_method":"{\"fetch_column\":32,\"fetch_table\":3}","hashcode":-1163436923,"use_column_name":"编号"},{"column_name":"上报人","expression":null,"fetch_method":"{\"fetch_column\":45,\"fetch_table\":4}","hashcode":189291496,"use_column_name":"上报人"},{"column_name":"待处理人","expression":null,"fetch_method":"{\"fetch_column\":48,\"fetch_table\":4}","hashcode":1950843179,"use_column_name":"待处理人"},{"column_name":"上报时间","expression":null,"fetch_method":"{\"fetch_column\":46,\"fetch_table\":4}","hashcode":-655180375,"use_column_name":"上报时间"},{"column_name":"异常时长","expression":null,"fetch_method":"{\"fetch_column\":43,\"fetch_table\":4}","hashcode":1878235238,"use_column_name":"异常时长"},{"column_name":"状态","expression":null,"fetch_method":"{\"fetch_column\":47,\"fetch_table\":4}","hashcode":-1499652246,"use_column_name":"状态"}],"top_n":null},{"biz_code":1,"default_dimension_type":null,"default_org_ids":null,"id":18384,"key":"1566791745164","name":"设备管理报修/维修实时任务表","states":[{"color":"#F5A623","status_id":"415"},{"color":"#bd10e0","status_id":"411"}],"style":null,"task_columns":[{"column_name":"名称","expression":null,"fetch_method":"{\"fetch_column\":31,\"fetch_table\":3}","hashcode":-318965052,"use_column_name":"名称"},{"column_name":"状态","expression":null,"fetch_method":"{\"fetch_column\":47,\"fetch_table\":4}","hashcode":-1499652246,"use_column_name":"状态"},{"column_name":"编号","expression":null,"fetch_method":"{\"fetch_column\":32,\"fetch_table\":3}","hashcode":-1163436923,"use_column_name":"编号"},{"column_name":"上报人","expression":null,"fetch_method":"{\"fetch_column\":45,\"fetch_table\":4}","hashcode":189291496,"use_column_name":"上报人"},{"column_name":"上一操作人","expression":null,"fetch_method":"{\"fetch_column\":41,\"fetch_table\":4}","hashcode":-727788316,"use_column_name":"上一操作人"},{"column_name":"上报时间","expression":null,"fetch_method":"{\"fetch_column\":46,\"fetch_table\":4}","hashcode":-655180375,"use_column_name":"上报时间"},{"column_name":"异常时长","expression":null,"fetch_method":"{\"fetch_column\":43,\"fetch_table\":4}","hashcode":1878235238,"use_column_name":"异常时长"}],"top_n":null}],"texts":[{"content":"<h1 style=\"color: rgb(249, 150, 59); text-align: center;\"><span style=\"font-weight: bold;\">秋田得数据， 用来测试模板2<br></span></h1>","id":18385,"key":"1566790906781","name":"","style":null},{"content":"<h1 style=\"color: rgb(206, 26, 26); text-align: center;\"><span style=\"font-weight: bold;\">Vae得数据， 用来测试模板3<br></span></h1>","id":18386,"key":"1566790906777","name":"","style":null}],"business":[{"default_dimension_type":null,"default_start_time":null,"default_end_time":null,"default_time_query_type":1,"id":18111,"key":"1566791222222","name":"业务组件-tNfR","style":null,"business_type":1,"status_id":"12345","status_name":"开关机","default_org_ids":null},{"default_dimension_type":null,"default_start_time":null,"default_end_time":null,"default_time_query_type":1,"id":18222,"key":"1566791333333","name":"业务组件-CupY","style":null,"business_type":2,"status_id":"123456","status_name":"点检","default_org_ids":null}],"theme":null}};

const list = {"message":"ok","code":"0000","data":[{"dashboard_id":8,"dashboard_name":"锅炉巡检仪表盘"},{"dashboard_id":11,"dashboard_name":"锅炉维修仪表盘"},{"dashboard_id":13,"dashboard_name":"王腾老铁正在测试**01234567890123456789"},{"dashboard_id":304,"dashboard_name":"新建仪表盘-测试报表配置3"},{"dashboard_id":306,"dashboard_name":"新建仪表盘-业务化影响"}]}

const texts = {"message":"ok","code":"0000","data":"<h1 style=\"color: rgb(249, 150, 59); text-align: center;\"><span style=\"font-weight: bold;\">秋田得数据， 用来测试模板2<br></span></h1>"};

const charts = {"message":"ok","code":"0000","data":{"name":"人员开机时长-柱状图","bar_data":{"series":[{"name":"开机时长（小时）","data":["136.48","8.97","270.03","543.82","135.4","18.12","271.23","273.02","135.88","274"],"type":1}],"xaxis":{"name":"人员","data":["熊克兰","周世全","霍定海","刘成","柏吉胜","封明扬","黄宗胜","赖阳琼","周永洪","夏蓉"]},"x_axis":{"name":"人员","data":["熊克兰","周世全","霍定海","刘成","柏吉胜","封明扬","黄宗胜","赖阳琼","周永洪","夏蓉"]},"lseries":[{"name":"开机时长（小时）","data":["136.48","8.97","270.03","543.82","135.4","18.12","271.23","273.02","135.88","274"],"type":1}],"rseries":[{"name":"开机时长（小时）","data":["136.48","8.97","270.03","543.82","135.4","18.12","271.23","273.02","135.88","274"],"type":1}]},"inverse_bar_data":{"series":[{"name":"开机时长（小时）","data":["136.48","270.03","543.82","135.4","271.23","273.02","135.88","274","271.98","273.52"]}],"yaxis":{"name":"人员","data":["熊克兰","霍定海","刘成","柏吉胜","黄宗胜","赖阳琼","周永洪","夏蓉","杜国印","张焱容"]},"y_axis":{"name":"人员","data":["熊克兰","霍定海","刘成","柏吉胜","黄宗胜","赖阳琼","周永洪","夏蓉","杜国印","张焱容"]}},"line_data":{"series":[{"name":"开机时长（小时）","data":["11664.73","20251.1","3489.5","2029.63","272.62","6507.45","4617.98","28061.07","4417.32","1482.78"]}],"xaxis":{"name":"使用部门","data":["四分厂制造一科","四分厂制造二科","四分厂制造三科","质管四科","仓储科","工装制造科","热处理科","二分厂制造一科","二分厂制造二科","质管二科"]},"x_axis":{"name":"使用部门","data":["四分厂制造一科","四分厂制造二科","四分厂制造三科","质管四科","仓储科","工装制造科","热处理科","二分厂制造一科","二分厂制造二科","质管二科"]}},"pie_data":{"data":[{"name":"万能外圆磨床","value":3},{"name":"外圆磨床","value":8},{"name":"加工中心","value":7},{"name":"立式加工中心","value":3},{"name":"单轴数控剃齿机","value":1},{"name":"数显双啮仪","value":1},{"name":"齿轮双面啮合综合测量仪","value":2},{"name":"充退磁机","value":1},{"name":"数控剃刀磨床","value":2},{"name":"清洗机炉","value":1}]},"table_data":{"data":[["3","1","TY特种设备物资3","TY的物资管理企业"],["3","1","TY特种设备物资3","TY的物资管理企业"],["4","1","TY特种设备物资4","测试机构2"],["10","1","TY测试报表物资10","默认部门"],["12","1","TY测试报表物资12","默认部门"]],"column_names":["物资编号","点检次数","物资名称","所属机构"]},"biaxial_data":{"rseries":[{"name":"维修用时（小时）","data":["15.42","10.4","6.15","17.67","0.53","4.32","1.05","38.17","1.98","5.77","6.68","6.35","1.4","9.25","9.75","14.47","3.63","6.18","0.17","3.03","1.93","0.82"]},{"name":"计划开机（小时/天）","data":[32,16,0,16,0,24,0,128,16,32,16,16,0,24,16,32,16,16,16,16,16,0]}],"xaxis":{"name":"名称","data":["外圆磨床","数控外圆磨床","数控插齿机","滚齿机","齿轮测量中心","连续渗碳炉生产线","金相试样切割机","数控车床","万能外圆磨床","半自动数控外圆磨床","齿轮倒角机","外圆磨床(无牌)","轴类数显齿轮双面啮合仪","数控蜗杆砂轮磨齿机","四柱压力机","四柱冷挤压机","履带式抛丸机","电动螺旋压力机","中频感应加热设备","开式固定台压力机","立式钻床","齿轮综合检查仪"]},"lseries":[{"name":"维修次数","data":[16,12,8,16,4,4,4,32,4,8,4,4,4,4,4,8,4,4,4,4,4,4]},{"name":"开机班次","data":[4,2,0,2,0,3,0,16,2,4,2,2,0,3,2,4,2,2,2,2,2,0]}],"x_axis":{"name":"名称","data":["外圆磨床","数控外圆磨床","数控插齿机","滚齿机","齿轮测量中心","连续渗碳炉生产线","金相试样切割机","数控车床","万能外圆磨床","半自动数控外圆磨床","齿轮倒角机","外圆磨床(无牌)","轴类数显齿轮双面啮合仪","数控蜗杆砂轮磨齿机","四柱压力机","四柱冷挤压机","履带式抛丸机","电动螺旋压力机","中频感应加热设备","开式固定台压力机","立式钻床","齿轮综合检查仪"]},"left_axis_type":null,"right_axis_type":null}}}

const states = {"message":"ok","code":"0000","data":869};

const business = {"message":"ok","code":"0000","data":11};

const tasks = {"message":"ok","code":"0000","data":{"column_names":["名称","状态","编号","上报人","上一操作人","上报时间","异常时长"],"data":[[{"content":"台式钻床","color":null},{"content":null,"color":null},{"content":"ZT0136","color":null},{"content":"张剑卿","color":null},{"content":"张剑卿","color":null},{"content":"09-02 11:33","color":null},{"content":"1天23小时","color":null}],[{"content":"卧式液压拉床","color":null},{"content":"待维修","color":"#bd10e0"},{"content":"LW0031","color":null},{"content":"张剑卿","color":null},{"content":"张剑卿","color":null},{"content":"08-22 14:20","color":null},{"content":"12天20小时","color":null}],[{"content":"五轴滚齿机","color":null},{"content":"维修中","color":"#F5A623"},{"content":"YG0265","color":null},{"content":"王小玉","color":null},{"content":"潘勇","color":null},{"content":"08-07 10:11","color":null},{"content":"28天","color":null}],[{"content":"数控外圆磨床","color":null},{"content":"维修中","color":"#F5A623"},{"content":"MW0230","color":null},{"content":"田洪志","color":null},{"content":"王明胜","color":null},{"content":"08-07 09:59","color":null},{"content":"28天","color":null}],[{"content":"电动螺旋压力机","color":null},{"content":"维修中","color":"#F5A623"},{"content":"PM0041","color":null},{"content":"叶克明","color":null},{"content":"罗义山","color":null},{"content":"08-07 09:39","color":null},{"content":"28天1小时","color":null}],[{"content":"滚齿机","color":null},{"content":"维修中","color":"#F5A623"},{"content":"YG0303","color":null},{"content":"唐海波","color":null},{"content":"陶长陵","color":null},{"content":"08-07 09:15","color":null},{"content":"28天1小时","color":null}],[{"content":"立式钻床","color":null},{"content":"维修中","color":"#F5A623"},{"content":"ZL0041","color":null},{"content":"彭淑芳","color":null},{"content":"罗义霞","color":null},{"content":"08-07 09:11","color":null},{"content":"28天1小时","color":null}],[{"content":"自动化数控滚齿机","color":null},{"content":"维修中","color":"#F5A623"},{"content":"YG0401","color":null},{"content":"蒋伟","color":null},{"content":"肖传杰","color":null},{"content":"08-07 09:11","color":null},{"content":"28天1小时","color":null}],[{"content":"准干切式滚齿机","color":null},{"content":"维修中","color":"#F5A623"},{"content":"YG0313","color":null},{"content":"代伟","color":null},{"content":"李鲜红","color":null},{"content":"08-07 09:06","color":null},{"content":"28天1小时","color":null}],[{"content":"托盘搬运车","color":null},{"content":"维修中","color":"#F5A623"},{"content":"VQ0316","color":null},{"content":"阳川","color":null},{"content":"高旭","color":null},{"content":"08-07 08:54","color":null},{"content":"28天1小时","color":null}],[{"content":"滚齿机","color":null},{"content":"维修中","color":"#F5A623"},{"content":"YG0316","color":null},{"content":"夏云淑","color":null},{"content":"王龙","color":null},{"content":"08-07 08:51","color":null},{"content":"28天2小时","color":null}],[{"content":"双头立式伺服深孔钻机","color":null},{"content":"维修中","color":"#F5A623"},{"content":"ZQ0015","color":null},{"content":"欧晓峰","color":null},{"content":"陈科1","color":null},{"content":"08-07 08:50","color":null},{"content":"28天2小时","color":null}],[{"content":"开式固定台压力机","color":null},{"content":"维修中","color":"#F5A623"},{"content":"PY0079","color":null},{"content":"刘元树","color":null},{"content":"马小龙","color":null},{"content":"08-07 08:41","color":null},{"content":"28天2小时","color":null}],[{"content":"除油槽","color":null},{"content":"维修中","color":"#F5A623"},{"content":"RG0020-1","color":null},{"content":"王述德","color":null},{"content":"余仁彬","color":null},{"content":"08-07 08:28","color":null},{"content":"28天2小时","color":null}],[{"content":"数控滚齿机","color":null},{"content":"维修中","color":"#F5A623"},{"content":"YG0427","color":null},{"content":"杨建","color":null},{"content":"蒋世涛","color":null},{"content":"08-07 08:24","color":null},{"content":"28天2小时","color":null}],[{"content":"外圆磨床","color":null},{"content":"维修中","color":"#F5A623"},{"content":"MW0122","color":null},{"content":"王晶","color":null},{"content":"段邦贵","color":null},{"content":"08-07 08:14","color":null},{"content":"28天2小时","color":null}],[{"content":"数控插齿机","color":null},{"content":"维修中","color":"#F5A623"},{"content":"YC0060","color":null},{"content":"钱德群","color":null},{"content":"侯志全","color":null},{"content":"08-07 08:10","color":null},{"content":"28天2小时","color":null}],[{"content":"半自动内圆磨床","color":null},{"content":"维修中","color":"#F5A623"},{"content":"MN0016","color":null},{"content":"吴家群","color":null},{"content":"颜权","color":null},{"content":"08-07 07:59","color":null},{"content":"28天2小时","color":null}],[{"content":"数控车床","color":null},{"content":"维修中","color":"#F5A623"},{"content":"CK0038","color":null},{"content":"莫红兰","color":null},{"content":"刘维见","color":null},{"content":"08-07 07:12","color":null},{"content":"28天3小时","color":null}],[{"content":"数控车床","color":null},{"content":"待维修","color":"#bd10e0"},{"content":"CK0154","color":null},{"content":"瞿凤容","color":null},{"content":"陈科1","color":null},{"content":"08-07 03:29","color":null},{"content":"28天7小时","color":null}],[{"content":"开式可倾压力机","color":null},{"content":"维修中","color":"#F5A623"},{"content":"PK0016","color":null},{"content":"杨芳云","color":null},{"content":"马小龙","color":null},{"content":"08-06 22:27","color":null},{"content":"28天12小时","color":null}],[{"content":"数控径向剃齿机","color":null},{"content":"维修中","color":"#F5A623"},{"content":"YT0026","color":null},{"content":"闻宗强","color":null},{"content":"吴立才","color":null},{"content":"08-06 16:13","color":null},{"content":"28天18小时","color":null}],[{"content":"数控外圆磨床","color":null},{"content":"维修中","color":"#F5A623"},{"content":"MW0231","color":null},{"content":"田小忠","color":null},{"content":"蓝庆","color":null},{"content":"08-06 16:02","color":null},{"content":"28天18小时","color":null}],[{"content":"数控外圆磨床","color":null},{"content":"维修中","color":"#F5A623"},{"content":"MW0205","color":null},{"content":"朱羽","color":null},{"content":"焦华","color":null},{"content":"08-06 14:49","color":null},{"content":"28天20小时","color":null}],[{"content":"五轴数控滚齿机","color":null},{"content":"维修中","color":"#F5A623"},{"content":"YG0362","color":null},{"content":"杨守清","color":null},{"content":"李益富","color":null},{"content":"08-06 14:24","color":null},{"content":"28天20小时","color":null}],[{"content":"数控外圆磨床","color":null},{"content":"维修中","color":"#F5A623"},{"content":"MW0217","color":null},{"content":"杨鑫","color":null},{"content":"李俊鹏","color":null},{"content":"08-06 13:35","color":null},{"content":"28天21小时","color":null}],[{"content":"数控车床","color":null},{"content":"待维修","color":"#bd10e0"},{"content":"CK0852","color":null},{"content":"苟孝洪","color":null},{"content":"王明胜","color":null},{"content":"08-06 08:21","color":null},{"content":"29天2小时","color":null}],[{"content":"万能螺纹磨床","color":null},{"content":"待维修","color":"#bd10e0"},{"content":"MQ0009","color":null},{"content":"赵学林","color":null},{"content":"明宗兵","color":null},{"content":"08-05 13:55","color":null},{"content":"29天20小时","color":null}],[{"content":"半自动内圆磨床","color":null},{"content":"待维修","color":"#bd10e0"},{"content":"MN0052","color":null},{"content":"蒋小莲","color":null},{"content":"李益富","color":null},{"content":"08-05 12:20","color":null},{"content":"29天22小时","color":null}],[{"content":"数控高速外圆磨床","color":null},{"content":"待维修","color":"#bd10e0"},{"content":"MW0261","color":null},{"content":"蒋开平","color":null},{"content":"王明胜","color":null},{"content":"08-05 11:23","color":null},{"content":"29天23小时","color":null}],[{"content":"数控车床","color":null},{"content":"维修中","color":"#F5A623"},{"content":"CK0922","color":null},{"content":"蒋开平","color":null},{"content":"王明胜","color":null},{"content":"08-05 11:22","color":null},{"content":"29天23小时","color":null}],[{"content":"剃齿机","color":null},{"content":"维修中","color":"#F5A623"},{"content":"YT0135","color":null},{"content":"张鹏川","color":null},{"content":"李鲜红","color":null},{"content":"08-05 08:21","color":null},{"content":"30天2小时","color":null}],[{"content":"自动校直机","color":null},{"content":"待维修","color":"#bd10e0"},{"content":"ME0018","color":null},{"content":"陈朝玲","color":null},{"content":"李强","color":null},{"content":"08-03 21:52","color":null},{"content":"31天12小时","color":null}],[{"content":"金相试样切割机","color":null},{"content":"待维修","color":"#bd10e0"},{"content":"JJ0015","color":null},{"content":"唐玉","color":null},{"content":"王正海","color":null},{"content":"08-02 13:16","color":null},{"content":"32天21小时","color":null}],[{"content":"滚齿机","color":null},{"content":"待维修","color":"#bd10e0"},{"content":"YG0346","color":null},{"content":"高春梅","color":null},{"content":"任艺","color":null},{"content":"08-01 09:22","color":null},{"content":"34天1小时","color":null}],[{"content":"万能工具铣床","color":null},{"content":"维修中","color":"#F5A623"},{"content":"XQ0014","color":null},{"content":"许卫兵","color":null},{"content":"明宗兵","color":null},{"content":"07-31 09:07","color":null},{"content":"35天1小时","color":null}],[{"content":"内圆磨床","color":null},{"content":"待维修","color":"#bd10e0"},{"content":"MN0047","color":null},{"content":"蒋强","color":null},{"content":"李强","color":null},{"content":"07-29 17:02","color":null},{"content":"36天17小时","color":null}],[{"content":"数控滚齿机","color":null},{"content":"待维修","color":"#bd10e0"},{"content":"YG0406","color":null},{"content":"刘本淑","color":null},{"content":"焦华","color":null},{"content":"07-20 15:53","color":null},{"content":"45天18小时","color":null}],[{"content":"数控车床","color":null},{"content":"待维修","color":"#bd10e0"},{"content":"CK0330","color":null},{"content":"李继东","color":null},{"content":"刘维见","color":null},{"content":"07-18 14:42","color":null},{"content":"47天20小时","color":null}],[{"content":"立式加工中心","color":null},{"content":"待维修","color":"#bd10e0"},{"content":"XK0055","color":null},{"content":"周杰亮","color":null},{"content":"李益富","color":null},{"content":"07-11 18:51","color":null},{"content":"54天16小时","color":null}]]}};

const group = {
    code: '0000',
    message: 'ok',
    data: {
        dashboard_group_id: 463,
        dashboard_group_name: "哈哈哈的仪表盘组可以的",
        dashboards: [
          {dashboard_id: 396, dashboard_name: "仪表盘、组件新增权限"},
          {dashboard_id: 412, dashboard_name: "人员与组织机构（人）维度"},
          {dashboard_id: 419, dashboard_name: "点检"},
          {dashboard_id: 431, dashboard_name: "任务组件"}
        ],
        delay: 45,
    }
}

const search = {
  "message":"",
  "code":"000000",
  "data":{
      "id":774,
      "name":"测试高级搜索",
      "theme":null,
      "charts":[
          {
              "id":42026,
              "name":"异常统计-副本",
              "key":"1583290952892",
              "style":null,
              "action_user_id":null,
              "data_rule":null,
              "data_rule_config_type":1,
              "chart_type":1,
              "report_config_id":27346,
              "model_no":1021,
              "is_single_dimension":1,
              "report_type":1,
              "top_n":null,
              "dimension_column_id":178198,
              "dimension_column_name":"编号",
              "chart_columns":[
                  {
                      "hashcode":5525161,
                      "lr":null,
                      "column_id":178200,
                      "column_name":"保养次数",
                      "use_column_name":"保养次数"
                  }
              ],
              "chart_conditions":[
                {
                  hashcode: 933131,
                  condition_id: 30070,
                  field: 'name',
                  type: 1,
                  condition_name: '名称',
                  use_condition_name: '名称',
                  default_selected_values: ['测试'],
                },
                {
                  hashcode: 933132,
                  condition_id: 30071,
                  field: 'level',
                  type: 2,
                  condition_name: '保养级别',
                  use_condition_name: '保养级别',
                  default_selected_values: [],
                },
                {
                  hashcode: 933133,
                  condition_id: 30072,
                  field: 'cycle',
                  type: 3,
                  condition_name: '保养周期',
                  use_condition_name: '保养周期',
                  default_selected_values: [1,2],
                },
                {
                  hashcode: 933134,
                  condition_id: 30073,
                  field: 'tree',
                  type: 4,
                  condition_name: '测试组织机构',
                  use_condition_name: '测试组织机构',
                  default_selected_values: [],
                },
              ],
              "sort_column_id":null,
              "order_by":null,
              "default_time_range_type":2,
              "time_query_type":4,
              "default_org_ids":null,
              "use_org_condition":1,
              "left_axis_type":null,
              "right_axis_type":null
          }
      ],
      "tasks":null,
      "states":null,
      "texts":null,
      "buss":null,
      "operator":"王徐巡",
      "operator_id":108,
      'lay_out': "{\"1583290952892\":{\"x\":-5,\"y\":-5,\"width\":1100,\"height\":730}}",
      "gantts":null
  }
}

const query_condition = [
  {
    'id': 30070,
    'field': 'name',
    'name': '名称',
    'type': 1,
    'dropdown_data': [],
    'default_value': null,
    'hash_code': 30101,
    'has_child': false
  },
  {
    'id': 30071,
    'field': 'level',
    'name': '保养级别',
    'type': 2,
    'dropdown_data': [
      {
        'value': 1,
        'show_text': '普通保养',
        'child': null
      },
      {
        'value': 2,
        'show_text': '润滑',
        'child': null
      },
      {
        'value': 3,
        'show_text': '一级保养',
        'child': null
      },
      {
        'value': 4,
        'show_text': '二级保养',
        'child': null
      },
      {
        'value': 5,
        'show_text': '三级保养',
        'child': null
      }
    ],
    'default_value': null,
    'hash_code': 30102,
    'has_child': false
  },
  {
    'id': 30072,
    'field': 'cycle',
    'name': '保养周期',
    'type': 3,
    'dropdown_data': [
      {
        'value': 1,
        'show_text': '日保养',
        'child': null
      },
      {
        'value': 2,
        'show_text': '周保养',
        'child': null
      },
      {
        'value': 4,
        'show_text': '月保养',
        'child': null
      },
      {
        'value': 5,
        'show_text': '季保养',
        'child': null
      },
      {
        'value': 6,
        'show_text': '半年保养',
        'child': null
      },
      {
        'value': 7,
        'show_text': '年保养',
        'child': null
      }
    ],
    'default_value': null,
    'hash_code': 30103,
    'has_child': false
  },
  {
    'id': 30073,
    'field': 'tree',
    'name': '测试组织机构',
    'type': 4,
    'dropdown_data': [],
    'default_value': null,
    'hash_code': 301088,
    'has_child': false
  },
]

export default { list, texts, charts, states, tasks, business, detail, group, search, query_condition };