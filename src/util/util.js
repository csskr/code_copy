/**
 * 常用的工具方法集合，每个方法必须添加详细的使用说明
 */

import qs from 'qs';

// 判断是否数组
export const isArray = obj => {
  return Object.prototype.toString.call(obj) === '[object Array]';
};

// 判断是否为对象
export const isObject = obj => {
  return Object.prototype.toString.call(obj) === '[object Object]';
};

// 判断是否为函数
export const isFunction = fun => {
  return fun && typeof fun === 'function';
};

// 判断是否为字符串
export const isString = str => {
  return str && typeof str === 'string';
};

// 判断是否为空对象
export const isEmptyObject = obj => {
  for (const key in obj) {
    return false;
  }
  return true;
};

/**
 * 返回一个 hash 值
 * @param {String | Number} len 长度
 * @return {String}
 */
export const getHash = len => {
  // return `r${Date.now()}d${Math.ceil(Math.random() * 1000)}`
  let length = Number(len);
  length = Number.isNaN(NaN) ? 8 : length; // 排除 NaN 的情况
  const arr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'.split('');
  const al = arr.length;
  let chars = '';
  while (length--) {
    chars += arr[parseInt(Math.random() * al, 10)];
  }
  return chars;
};

/**
 * 数字每三位加一个逗号
 * @param {String | Number} n 原始数字
 * @return {String} 格式化的数字
 */
export const formatNum = (n = 0) => {
  let result = [];
  let counter = 0;
  let num = n.toString().split('');
  for (let i = num.length - 1; i >= 0; i--) {
    counter++;
    result.unshift(num[i]);
    if (!(counter % 3) && i !== 0) {
      result.unshift(',');
    }
  }
  return result.join('');
};

/**
 * url 参数加密
 * @param {Object} params
 */
export const btoaQuery = params => {
  if (isObject(params)) {
    try {
      return window.encodeURIComponent(window.btoa(qs.stringify(params)));
    } catch (err) {
      return false;
    }
  }
};

/**
 * url 参数解密
 * @param {String} str
 */
export const atobQuery = str => {
  if (isString(str)) {
    try {
      return qs.parse(window.atob(window.decodeURIComponent(str)));
    } catch (err) {
      return false;
    }
  }
};

/**
 * 短线命名转驼峰命名
 * 'wise-wrong' -> 'wiseWrong'
 * @param {String} s
 */
export const kebabToCamel = s => {
  return s.replace(/-(\w)/g, (_, c) => c.toUpperCase());
};

/**
 * 驼峰命名转短线命名
 * 'WiseWrong' -> 'wise-wrong'
 * @param {String} s
 */
export const camelToKebab = s => {
  return s.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
};

/**
 * 简易版深拷贝
 * 会丢失函数与 undefined、symbol
 * @param {*} v
 */
export const copyByJson = v => {
  return JSON.parse(JSON.stringify(v));
};

/**
 * 防抖
 * @param {Function} fn 函数
 * @param {Number} delay 延时
 */
export const debounce = (fn, delay) => {
  let timer = null;
  return function() {
    const self = this;
    const args = arguments;
    clearTimeout(timer);
    timer = setTimeout(() => {
      fn.apply(self, args);
    }, delay);
  };
};

/**
 * 节流
 * @param {Function} fn 函数
 * @param {Number} wait 等待时间
 * @param {Number} delay 延时
 */
export const throttle = (fn, wait, delay) => {
  let timer = null;
  let previous = null;

  return function() {
    const self = this;
    const args = arguments;
    const now = Date.now();
    if (!previous) previous = now;
    if (now - previous > wait) {
      fn.apply(self, args);
      previous = now;
    } else if (delay) {
      clearTimeout(timer);
      timer = setTimeout(() => {
        fn.apply(self, args);
      }, delay);
    }
  };
};

/**
 * 存入 sessionStorage
 * @param {String} key 变量名
 * @param {*} value 值
 */
export const saveToStorage = (key, value) => {
  if (key && sessionStorage && sessionStorage.setItem) {
    sessionStorage.setItem(key, JSON.stringify(value));
  }
};

/**
 * 从 sessionStorage 取
 * @param {String} key 变量名
 */
export const getFormStorage = key => {
  if (key && sessionStorage && sessionStorage.getItem) {
    return JSON.parse(sessionStorage.getItem(key));
  }
};

/**
 * 从 sessionStorage 移除
 * @param {String} key 变量名
 */
export const removeFormStorage = key => {
  if (key && sessionStorage && sessionStorage.removeItem) {
    sessionStorage.removeItem(key);
  }
};

/**
 * 将字符串截取头尾，中间用省略号代替
 * 如：'我们的恋爱是对生命的严重浪费' 处理为 '我们…浪费'
 * @param {String} str 需要处理的字符串
 * @param {Number} head 保留头部字符串的长度
 * @param {Number} tail 保留尾部字符串的长度
 */
export const beautySubStr = (str, head, tail) => {
  const h = Number(head);
  const t = Number(tail);
  const s = str.length;
  const len = h + t;
  if (!str || !len || len + 1 >= s) {
    return str;
  }
  const headStr = str.substr(0, h);
  const tailStr = str.substr(s - t, t);
  return `${headStr}…${tailStr}`;
};

/**
 * 根据 value 值从对象数组中找到对应的对象
 * @param {*} value
 * @param {*} dropdown 对象数组
 * @param {*} valueKey value 值在对象中的 key
 */
export const getOptionByValue = (value, dropdown, valueKey = 'value') => {
  if (!isArray(dropdown) || !value) {
    return;
  }
  for (const obj of dropdown) {
    if (`${obj[valueKey]}` === `${value}`) {
      return obj;
    }
  }
};

/**
 * 提取对象数组中的指定 key 组成新数组
 * @param {*} array
 * @param {*} key
 */
export const getArrayValue = (array, key = 'value') => {
  if (!isArray(array)) {
    return;
  }
  const res = [];
  if (array) {
    array.forEach(t => {
      res.push(t[key]);
    });
  }
  return res;
};

/**
 * 将毫秒格式化为天、时、分、秒
 * @param {Number} t 毫秒
 */
export const formatDuring = t => {
  const HOUR = 1000 * 60 * 60;
  const d = parseInt(t / (HOUR * 24));
  const h = parseInt((t % (HOUR * 24)) / HOUR);
  const m = parseInt((t % HOUR) / (1000 * 60));
  // const s = parseInt((t % (1000 * 60)) / 1000);

  let text = '';
  d && (text += '${d}天');
  h && (text += '${h}小时');
  m && (text += '${m}分');
  // s && (text += `${s}秒');
  return text || '-';
};

/**
 * 数组去重
 * @param {Array} arr 对象数组
 * @param {String} valueKey 去重key
 */
export const arrayRemoval = (arr, valueKey) => {
  let newArr = [];
  if (isArray(arr)) {
    let obj = {};
    newArr = arr.reduce((pre, cur) => {
      if (!obj[cur[valueKey]]) {
        obj[cur[valueKey]] = true;
        pre.push(cur);
      }
      return pre;
    }, []);
  }
  return newArr;
};

/**
 * 判断对象是否相等
 * @param {origin} obj 第一个对象
 * @param {target} obj 第二个对象
 */
export const compare=(origin, target)=>{
  if (typeof target !== "object") {
    //target不是对象/数组
    return origin === target; //直接返回全等的比较结果
  }

  if (typeof origin !== "object") {
    //origin不是对象/数组
    return false; //直接返回false
  }
  for (let key of Object.keys(target)) {
    //遍历target的所有自身属性的key
    if (!compare(origin[key], target[key])) {
      //递归比较key对应的value，
      //value不等，则两对象不等，结束循环，退出函数，返回false
      return false;
    }
  }
  //遍历结束，所有value都深度比较相等，则两对象相等
  return true;
}