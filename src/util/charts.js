/**
 * 图表工具方法
 */

import { isObject, isArray } from './util';

export const initTooltipItem = (params) => {
  const { value, marker, seriesName, color } = params;
  // 颜色为对象时，为渐变颜色，需要手动拼接
  let mark = marker;
  if (isObject(color)) {
    const { colorStops = [] } = color;
    const endColor = colorStops[0] && colorStops[0].color;
    const startColor = colorStops[1] && colorStops[1].color;
    const colorStr = `background-image: linear-gradient(0deg, ${startColor}, ${endColor});`;
    mark = `
      <span style="
        display:inline-block;
        margin-right:5px;
        border-radius:10px;
        width:10px;
        height:10px;
        ${colorStr}
      "></span>`;
  }
  return `<div>${mark}${seriesName}: ${value}</div>`;
};

// tooltip 格式化方法，用于展示渐变色
export const formatterTooltip = (params) => {
  let str = '';
  if (params && isArray(params)) {
    str = `<div>${params[0].name}</div>`;
    params.map(item => {
      str += `\n ${initTooltipItem(item)}`;
    });
  }
  return str;
};
