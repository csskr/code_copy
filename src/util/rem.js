// import { ROOT_VALUE } from '@/constate/style';

export function setRemInit() {
  // postcss-px2rem的内容
  // 基准大小
  // const baseSize = ROOT_VALUE;
  // 设置 rem 函数
  function setRem() {
    // let rootHtml = document.documentElement;
    // // 限制展现页面的最小宽度（暂时只考虑PC，后续要判断移动端）
    // let rootWidth = rootHtml.clientWidth < 1280 ? 1280 : rootHtml.clientWidth;
    // // 当前页面宽度相对于 1920 px(设计稿尺寸)的缩放比例，可根据自己需要修改。
    // const scale = rootWidth / 7680;
    // // 设置页面根节点字体大小
    // document.documentElement.style.fontSize = `${baseSize * scale}px`;
    let designSize = 1920; // 设计图尺寸
    let html = document.documentElement;
    let wW = html.clientWidth; // 窗口宽度
    let rem = wW * 100 / designSize;
    document.documentElement.style.fontSize = rem + 'px';
  }
  // 初始化
  setRem();
  // 改变窗口大小时重新设置 rem
  window.addEventListener('resize', setRem);
}
