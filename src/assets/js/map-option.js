//map-option.js

export default {
  tooltip: {
    trigger: 'item',
    show:false,
    backgroundColor:'opacity',
    formatter:  (params)=> {
      console.log(params)
      return params.data.name + ' : ' + params.data.value;
    }
  },
  dataRange: {
    color: ['#EF3F01','#F4D400','#00D8BF','#005BD8'],
    // min: 0,
    // max: 2500,
    x: 'left',
    y: 'bottom',
    bottom:10,
    calculable: false,
    itemWidth:70,
    itemHeight:450,
    text: ['','热度值'],
    label: {
      show: true,
      color: '#FFF',
    },
    textStyle:{
      fontSize:50,
      color:'#FFF'
    }
  },
  grid:{
    top:10,
    left:10
  },
  // 地图样式
  geo: {
    map: 'china',   // 地图样式，当为‘北京’时，会显示北京地图
    roam : false,
    aspectScale:50,
    layoutSize:900,
    label: {
      emphasis: {
        show: true
      }
    },
    zoom : 5,     //  初始大小
    scaleLimit : {
      min : 1,  //  最小缩放
      max : 50     //  最大缩放
    },
    // regions : regions(data)     // 省份样式方法
  },
  series: [
    {
      type: 'map',
      mapType: 'map',
      encode: {
        value : 2
      },
      coordinateSystem: 'geo',//
      hoverAnimation: true,
      roam: false,
      symbolSize: 3,//点大小
      zoom:1.25,
      label: {
        normal: {
          textStyle: {
            fontSize: 50,
            fontWeight: 'bold',
          }
        }
      },
      itemStyle:{
        normal: {
          borderWidth: 1,//区域边框宽度
          borderColor: '#B4D1F9',//区域边框颜色
          areaColor:"rgba(0,0,0,0)",//区域颜色
          label:{
            show: true,
            color:'#FFF',

            formatter: function (params) {
              return params.name+"\n"+ params.value;    //地图上展示文字 + 数值
            },
          }
        },
        emphasis: {
          borderWidth: 2,
          borderColor: '#16FFF1',
          areaColor:"#CEB618",
          label:{
            show: true,
            color:'#FFF',
          }
        }
      },
      data: []
    }
  ]
};
