// function showMap(allData, areaid = 310000) {
//     var distribution_fb = echarts.init(document.getElementById('distribution_fb'));
//     //各省份的地图json文件
//     var provinces = {
//         上城区: "/Public/home/yq/src/map/330102.json",
//         下城区: "/Public/home/yq/src/map/330103.json",
//         拱墅区: "/Public/home/yq/src/map/330105.json",
//         萧山区: "/Public/home/yq/src/map/330109.json",
//         滨江区: "/Public/home/yq/src/map/330108.json",
//         桐庐县: "/Public/home/yq/src/map/330122.json",
//         西湖区: "/Public/home/yq/src/map/330106.json",
//         淳安县: "/Public/home/yq/src/map/330127.json",
//         余杭区: "/Public/home/yq/src/map/330110.json",
//         江干区: "/Public/home/yq/src/map/330104.json",
//         建德市: "/Public/home/yq/src/map/330182.json",
//         临安区: "/Public/home/yq/src/map/330185.json",
//         富阳区: "/Public/home/yq/src/map/330111.json",

//         建安区: "/Public/home/yq/src/map/411003.json",
//         襄城县: "/Public/home/yq/src/map/411025.json",
//         长葛市: "/Public/home/yq/src/map/411082.json",
//         魏都区: "/Public/home/yq/src/map/411002.json",
//         禹州市: "/Public/home/yq/src/map/411081.json",
//         鄢陵县: "/Public/home/yq/src/map/411024.json",
//     };
//     var max = 0; //最大值

//     //	$.each(allData, function(i, n) {
//     //		if(n.value > max) {
//     //			max = n.value;
//     //		}
//     //	});

//     let themeIndex;
//     let themeColor = ['#06bffe', '#0badf6', '#1199ed', '#168be6', '#1e72db', '#245ed2'];
//     let shadowcolor = '#0c3077';
//     let cookies = document.cookie;
//     let list = cookies.split(";");
//     for (let i = 0; i < list.length; i++) {
//         let arr = list[i].split('=')
//         if (arr[0].trim() == 'yq_theme_index') {
//             themeIndex = arr[1]
//             if (arr[1] == 0) {
//                 themeColor = ['#06bffe', '#0badf6', '#1199ed', '#168be6', '#1e72db', '#245ed2']
//                 shadowcolor = '#0c3077'
//             } else if (arr[1] == 1) {
//                 themeColor = ['#ffcaa7', '#ffc096', '#ffa265', '#ff8f45', '#ff7a21', '#ff6600']
//                 shadowcolor = '#482814'
//             } else {
//                 themeColor = ['#2be2bd', '#23c69a', '#26ae95', '#18967f', '#158d64', '#0f7b56']
//                 shadowcolor = '#04362d'
//             }
//         }
//     }

//     let areaidd;
//     $.get('/index.php/vision/getareaid', function(res) {
//         if (res.status == '1') {

//             areaidd = res.data.areaid


//             loadMap('/Public/home/yq/src/map/' + areaidd + '.json', 'hangzhou'); //初始化地图

//         } else {
//             alert(res.msg)
//         }
//     })





//     // console.log(allData)

//     var timeFn = null;
//     var scaleToggle = true;
//     var scaleNum = '1.1';
//     var mapadd = '/Public/home/yq/src/map/310000.json';
//     var mapName = 'hangzhou';
//     //单击切换到地图，当mapCode有值,说明可以切换到下级地图
//     distribution_fb.on('click', function(params) {

//         //由于单击事件和双击事件冲突，故单击的响应事件延迟250毫秒执行
//         var name = params.name; //地区name
//         var mapCode = provinces[name]; //地区的json数据
//         var area_id = areaId[name]; //地区的id
//         // console.log(area_id)
//         if (!mapCode) {
//             // alert('无此区域地图显示');
//             return;
//         }
//         let new_arr = getTabData(area_id);

//         mapadd = mapCode;
//         mapName = name;

//         $('.distribution').find('div.mapback').show(300);
//         setTimeout(function() {
//             let maxnum = $('#distri_ul>li>span').eq(1).html();
//             // console.log(maxnum)
//             loadMap(mapCode, name, new_arr, maxnum);
//         }, 300)
//     });

//     // 乡镇返回到区县
//     $('.distribution').find('div.mapback').click(function() {
//         clearTimeout(timeFn);
//         let cookie_areaid = localStorage.getItem('areaid');
//         // console.log(cookie_areaid)

//         //返回全国地图
//         loadMap('/Public/home/yq/src/map/' + cookie_areaid + '.json', 'hangzhou');
//         getTabData(cookie_areaid);
//         mapadd = '/Public/home/yq/src/map/' + cookie_areaid + '.json';
//         mapName = 'hangzhou';


//         $(this).hide(300)
//     })

//     // 绑定双击事件，返回全国地图
//     distribution_fb.on('dblclick', function(params) {
//         let cookie_areaid = localStorage.getItem('areaid');
//         //当双击事件发生时，清除单击事件，仅响应双击事件
//         clearTimeout(timeFn);

//         //返回全国地图
//         loadMap('/Public/home/yq/src/map/' + cookie_areaid + '.json', 'hangzhou');
//         getTabData(cookie_areaid);
//         mapadd = '/Public/home/yq/src/map/' + cookie_areaid + '.json';
//         mapName = 'hangzhou';

//         $('.distribution').find('div.mapback').hide(300)
//     });

//     // 拖拽和缩放事件
//     distribution_fb.on('georoam', function(params) {
//         if (scaleNum == '1.1') {
//             scaleNum = '0'
//             loadMap(mapadd, mapName)
//         }
//     });

//     function mapreload() {
//         $("#distribution").load("distribution.html");
//         let cookies = document.cookie;
//         let list = cookies.split(";");
//         for (let i = 0; i < list.length; i++) {
//             let arr = list[i].split('=')
//             if (arr[0].trim() == 'mapaddress') {
//                 mapadd = arr[1]
//             } else if (arr[0].trim() == 'mapName') {
//                 mapName = arr[1]
//             }
//         }
//         loadMap(mapadd, mapName);
//     }

//     function loadMap(mapCode, name, dataer = allData, maxNum) {
//         for (let i = 0; i < dataer.length; i++) {
//             if (dataer[i].value > max) {
//                 max = dataer[i].value
//             }
//         }
//         $.get(mapCode, function(data) {
//             if (data) {
//                 echarts.registerMap(name, data);
//                 var option = {
//                     tooltip: {
//                         show: false,
//                         formatter: function(params) {
//                             if (params.data)
//                                 return params.name + '：' + params.data['value']
//                         },
//                     },
//                     visualMap: {
//                         type: 'continuous',
//                         min: 0,
//                         max: maxNum || max,
//                         text: ['热', '淡'],
//                         zlevel: 80,
//                         top: "360",
//                         left: "20",
//                         textStyle: {
//                             color: "#fff"
//                         },
//                         itemWidth: 10,
//                         showLabel: true,
//                         inRange: {
//                             color: themeColor
//                         },
//                         splitNumber: 0
//                     },
//                     geo: {
//                         show: true,
//                         map: name,
//                         zoom: 1.2,
//                         zlevel: 20,
//                         scaleLimit: {
//                             min: 0.01,
//                             max: 1.8
//                         },
//                         label: {
//                             normal: {
//                                 show: false
//                             },
//                             emphasis: {
//                                 show: false
//                             }
//                         },
//                         roam: false,
//                         silent: 'true',
//                         emphasis: {
//                             show: false
//                         },
//                         itemStyle: {
//                             normal: {
//                                 areaColor: shadowcolor,
//                                 borderWidth: 0,
//                                 shadowColor: shadowcolor,
//                                 shadowBlur: 0,
//                                 shadowOffsetX: 1,
//                                 shadowOffsetY: 20
//                             }
//                         },
//                         regions: [{
//                             name: '禹州市',
//                             itemStyle: {
//                                 areaColor: 'red',
//                                 color: 'red'
//                             },
//                             label: {
//                                 normal: {
//                                     position: 'bottom',
//                                     distance: 40
//                                 }
//                             }
//                         }]
//                     },
//                     series: [{
//                         name: 'MAP',
//                         type: 'map',
//                         roam: 'false',
//                         zlevel: 50,
//                         mapType: name,
//                         data: dataer,
//                         zoom: 1.2,
//                         selectedMode: 'false', //是否允许选中多个区域
//                         scaleLimit: {
//                             min: .6,
//                             max: 2.5
//                         },
//                         label: {
//                             emphasis: {
//                                 show: true
//                             },
//                             normal: {
//                                 show: true,
//                                 formatter: function(val) {
//                                     var area_content = null;
//                                     if (val.value) {

//                                         area_content = '{a|' + val.name + '}' + '-' + '{b|' + val.value + '}';
//                                     } else {
//                                         area_content = '{a|' + val.name + '}' + '-' + '{b|' + 0 + '}';
//                                     }
//                                     if (val.name == '魏都区') {
//                                         // alert('123')
//                                     }
//                                     // var area_content = '{a|'+ val.name +'}' + '-' + '{b|'+ val.value +'}';
//                                     return area_content.split("-").join("\n");
//                                 },
//                                 rich: {
//                                     a: {
//                                         color: '#fff',
//                                         fontSize: 10,
//                                         lineHeight: 20
//                                     },
//                                     b: {
//                                         color: "#fff",
//                                         fontSize: 10,
//                                         lineHeight: 20
//                                     }
//                                     //									b: {
//                                     //										height: 16,
//                                     //										backgroundColor: {
//                                     //											image: ''
//                                     //										}
//                                     //									},
//                                 }, //富文本样式,就是上面的formatter中'{a|'和'{b|'
//                                 textStyle: {
//                                     fontWeight: 'bold',
//                                     color: 'red'
//                                 }
//                             }
//                         }, //地图中文字内容及样式控制
//                         itemStyle: {
//                             borderColor: '#fff',
//                             borderWidth: 0
//                         }
//                     }, {
//                         name: 'MAP',
//                         type: 'map',
//                         width: 16,
//                         heigth: 16,
//                         backgroungColor: {
//                             image: '/Public/home/yq/images/visionimg/848.png'
//                         },
//                         symbol: '/Public/home/yq/images/visionimg/848.png',
//                         symbolSize: [16],
//                     }],
//                 };
//                 distribution_fb.clear();
//                 setTimeout(function() {
//                     distribution_fb.setOption(option);
//                 }, 500)

//                 // curMap = {
//                 //     mapCode: mapCode,
//                 //     mapName: name
//                 // };
//             } else {
//                 alert('加载到最下边啦');
//             }
//         });
//     }
// }