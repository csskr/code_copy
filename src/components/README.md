### 组件说明文档

| 名称         | 简介       | 参数       |
| ----------- | ---------- | --------- |
| EmergentEvent | 应急事件 | <code>{data}</code> |
| EmotionalTrend | 情感趋势 | <code>{data}</code> |
| EmotionsAnalysis | 情感分析 | <code>{data}</code> |
| GLMap | 三围地图 | <code>{data}</code> |
| LivelyMedia | 活跃媒体 | <code>{data}</code> |
| MediaAnalysis | 媒体分析 | <code>{data}</code> |
| NewsEvent | 最新事件 | <code>{data}</code> |
| PublicSentimentHot | 舆情热度 | <code>{data}</code> |