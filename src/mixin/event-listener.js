/**
 * 遥控器事件监听
 */
import { WEBVIEW_VAR, DIRECTION_FLAG, TV_EVENTS } from '../constate/dashboard/index';


export default {
  data() {
    return {};
  },
  mounted() {
    this.initListener();
  },
  methods: {
    // 判断是否要执行初始化事件（因为仪表盘组到普通仪表盘会触发mounted）
    isOperateInit() {
      // 点击之前的currentId
      let firstMenuId = this.stores.firstMenuId;
      // 新点击的currentId
      const newCurrentId = this.stores.currentMenuId;

      const dashboards = (this.stores.dashboards.length > 0) && this.stores.dashboards[firstMenuId];
      const is_group = dashboards && dashboards.is_group;
      const newDashboards = (this.stores.dashboards.length > 0) && this.stores.dashboards[newCurrentId];
      const new_is_group = newDashboards && newDashboards.is_group;
      // 判断 除了普通页和group页之间切换的情况
      const flag = !(parseInt(is_group) !== parseInt(new_is_group));
      // 同时判断 新切换的页面在普通页（this.isBigSize为true是group页）
      return flag && !this.isBigSize;
    },
    // 鼠标click事件
    menuItemClick(index) {
      // 将事件抛出到group页面
      this.$emit('menuItemClick', index);
      // 重新加载
      const flag = this.isOperateInit();
      flag && this.init(index);
    },
    // 监听遥控器按键事件
    initListener() {
      window.addEventListener(TV_EVENTS.BACK, this.escEventHandler);
      window.addEventListener(TV_EVENTS.ENTER, this.enterEventHandler);
      window.addEventListener(TV_EVENTS.TOP, this.directionEventHandlerTop);
      window.addEventListener(TV_EVENTS.BOTTOM, this.directionEventHandlerBottom);
      window.addEventListener(TV_EVENTS.LEFT, this.directionEventHandlerLeft);
      window.addEventListener(TV_EVENTS.RIGHT, this.directionEventHandlerRight);
    },
    // 遥控器 - 方向键
    directionEventHandlerTop() {
      this.$store.dashboardModel.commit('directionEventHandler', DIRECTION_FLAG.TOP);
    },
    directionEventHandlerBottom() {
      this.$store.dashboardModel.commit('directionEventHandler', DIRECTION_FLAG.BOTTOM);
    },
    directionEventHandlerLeft() {
      this.$store.dashboardModel.commit('directionEventHandler', DIRECTION_FLAG.LEFT);
    },
    directionEventHandlerRight() {
      this.$store.dashboardModel.commit('directionEventHandler', DIRECTION_FLAG.RIGHT);
    },
    // 遥控器 - 确认键
    enterEventHandler() {
      const dashboardStates = this.$store.dashboardModel.states;
      if (dashboardStates.dialogVisible || dashboardStates.styleDialogVisible) { return; }
      if (dashboardStates.menuDialogVisible) {
        // 菜单选中事件
        this.$store.dashboardModel.update('isSelectMenu', true);
        const currentId = dashboardStates.currentMenuId;
        // 重新加载
        const flag = this.isOperateInit();
        flag && this.init(currentId);
        if (dashboardStates.firstMenuId !== currentId) {
          // 在客户端缓存当前选中的仪表盘id信息
          const { child, is_group, dashboard_id } = dashboardStates.dashboards[currentId] || {};
          const isGroup = child && Number(is_group) === 1;

          this.$store.commit('SaveAppData', {
            key: WEBVIEW_VAR.DASHBOARD_ID,
            value: String(dashboard_id)
          });
          this.$store.commit('SaveAppData', {
            key: WEBVIEW_VAR.IS_DASHBOARD_GROUP,
            value: String(isGroup)
          });
          // 清空之前保存的筛选参数字典并在客户端缓存
          this.$store.dashboardModel.commit('emptyConditionMap');
          this.$store.dashboardModel.commit('emptyStyleMap');
          this.$store.commit('SaveAppData', {
            key: WEBVIEW_VAR.DASHBOARD_CONDITIONS,
            value: dashboardStates.conditionMap && JSON.stringify(dashboardStates.conditionMap)
          });
        }
        this.$store.dashboardModel.commit('escEventHandler');
      } else {
        this.$store.dashboardModel.update('isSelectMenu', false);
        // 上次退出时的仪表盘 id  如果没值 就赋值第一个 dashboards[0].dashboard_id
        // let id = this.commit('GetAppData', WEBVIEW_VAR.DASHBOARD_ID); // 从缓存上取id
        const { gid, id } = this.$router.currentRoute.params || {}; // 从url上取id
        let theId = id || gid;
        if (!theId) {
          const { dashboards } = this.$store.dashboardModel.states;
          theId = dashboards[0].dashboard_id;
        }
        this.$store.dashboardModel.commit('enterEventHandler', Number(theId));
      }
    },
    // 遥控器 - 返回键
    escEventHandler() {
      this.$store.dashboardModel.commit('escEventHandler');
    },
    // 销毁事件监听
    destroyListener() {
      window.removeEventListener(TV_EVENTS.BACK, this.escEventHandler);
      window.removeEventListener(TV_EVENTS.ENTER, this.enterEventHandler);
      window.removeEventListener(TV_EVENTS.TOP, this.directionEventHandlerTop);
      window.removeEventListener(TV_EVENTS.BOTTOM, this.directionEventHandlerBottom);
      window.removeEventListener(TV_EVENTS.LEFT, this.directionEventHandlerLeft);
      window.removeEventListener(TV_EVENTS.RIGHT, this.directionEventHandlerRight);
    }
  },
};
