/**
 * 利用 dataZoom 实现图表自动滚动
 */

export default {
  data() {
    return {
      dataZoomEndVal: null,
      dataZoomStartVal: null,
      Timer: null,
      pageSize: 0, // 每页可以展示的类目数量
    };
  },
  props: {
    // 尺寸: large 大尺寸，nomarl 普通尺寸
    size: {
      type: String,
      default: 'nomarl'
    },
  },
  mounted() {
    this.isBigSize && this.setPageSize();
  },
  methods: {
    // 自动滚动
    dataZoomAutoScoll() {
      this.destroyTimer();
      this.dataZoomStartVal = 0;
      this.dataZoomEndVal = this.pageSize - 1;
      this.Timer = setInterval(() => {
        const max = this.total && this.total - 1;
        if (
          this.dataZoomEndVal > max ||
          this.dataZoomStartVal > max - this.pageSize
        ) {
          this.dataZoomStartVal = 0;
          this.dataZoomEndVal = this.pageSize - 1;
        } else {
          this.dataZoomStartVal += 1;
          this.dataZoomEndVal += 1;
        }
      }, 2000);
    },
    // 清空定时器
    destroyTimer() {
      clearInterval(this.Timer);
      this.Timer = null;
    },
  },
  computed: {
    isBigSize() {
      return this.size === 'large';
    },
    dataZoomOptionsX() {
      return [
        {
          type: 'slider',
          xAxisIndex: 0,
          zoomLock: true,
          bottom: -10,
          startValue: this.dataZoomStartVal,
          endValue: this.dataZoomEndVal,
          handleSize: 0,
          borderColor: 'transparent',
          backgroundColor: 'transparent',
          fillerColor: 'transparent',
          showDetail: false,
        },
        {
          type: 'inside',
          id: 'insideX',
          xAxisIndex: 0,
          startValue: this.dataZoomStartVal,
          endValue: this.dataZoomEndVal,
          zoomOnMouseWheel: false,
          moveOnMouseMove: true,
          moveOnMouseWheel: true,
        }
      ];
    },
    dataZoomOptionsY() {
      return [
        {
          type: 'slider',
          yAxisIndex: 0,
          zoomLock: true,
          bottom: -10,
          startValue: this.dataZoomStartVal,
          endValue: this.dataZoomEndVal,
          handleSize: 0,
          borderColor: 'transparent',
          backgroundColor: 'transparent',
          fillerColor: 'transparent',
          showDetail: false,
        },
        {
          type: 'inside',
          id: 'insideX',
          yAxisIndex: 0,
          startValue: this.dataZoomStartVal,
          endValue: this.dataZoomEndVal,
          zoomOnMouseWheel: false,
          moveOnMouseMove: true,
          moveOnMouseWheel: true,
        }
      ];
    },
  }
};
